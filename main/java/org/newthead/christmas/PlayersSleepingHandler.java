package org.newthead.christmas;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;

import org.newthead.christmas.tileentities.TileEntityChristmasPresent;
import org.newthead.christmas.tileentities.TileEntityStocking;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent.Phase;
import cpw.mods.fml.common.gameevent.TickEvent.WorldTickEvent;

public class PlayersSleepingHandler
{
	private Random random = new Random();
	public final static PlayersSleepingHandler instance = new PlayersSleepingHandler();

	@SubscribeEvent
	public void onWorldTick(WorldTickEvent event)
	{
		if (event.phase == Phase.START)
		{
			if (!ChristmasCraft.isChristmasTime())
			{
				return;
			}
			boolean isChristmasDay = ChristmasCraft.isChristmasDay();
			WorldServer world = (WorldServer)event.world;
			if (world.areAllPlayersAsleep())
			{
				Object[] tiles = world.loadedTileEntityList.toArray();
				for (int i = 0; i < tiles.length; i++)
				{
					TileEntity tile = (TileEntity)tiles[i];
					Block block = world.getBlock(tile.xCoord, tile.yCoord, tile.zCoord);
					if (block == ChristmasCraft.blockStocking)
					{
						ItemStack present = ChristmasCraft.getRandomStockingGift();
						if (((TileEntityStocking)tile).getContents() == null)
						{
							((TileEntityStocking)tile).setContents(present);
						}
					}
					if (isChristmasDay && block == ChristmasCraft.blockTreestand)
					{
						generatePresentsAroundTree(world, tile.xCoord, tile.yCoord, tile.zCoord);
					}
				}
			}
		}
	}

	private void generatePresentsAroundTree(World world, int x, int y, int z)
	{
		if (world.getBlock(x, y, z) != ChristmasCraft.blockTreestand)
		{
			return;
		}
		if (world.getBlock(x, y + 1, z) != Blocks.log)
		{
			return;
		}
		for (int xOffset = -1; xOffset <= 1; xOffset++)
		{
			for (int zOffset = -1; zOffset <= 1; zOffset++)
			{
				if (ChristmasCraft.blockPresent.canPlaceBlockAt(world, x + xOffset, y, z + zOffset) && this.random.nextBoolean())
				{
					world.setBlock(x + xOffset, y, z + zOffset, ChristmasCraft.blockPresent);
					TileEntityChristmasPresent tile = (TileEntityChristmasPresent)world.getTileEntity(x + xOffset, y, z + zOffset);
					if (tile == null)
					{
						tile = new TileEntityChristmasPresent();
						world.setTileEntity(x + xOffset, y, z + zOffset, tile);
					}
					tile.setContents(ChristmasCraft.getRandomTreeGift());
				}
			}
		}
	}
}
