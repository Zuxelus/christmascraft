package org.newthead.christmas;

import java.io.File;

import net.minecraftforge.common.config.Configuration;
import cpw.mods.fml.client.event.ConfigChangedEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;

public class ConfigurationHandler
{
	public Configuration configuration;

	public void init(File configFile)
	{
		if (configuration == null)
		{
			configuration = new Configuration(configFile);
		}
		loadConfiguration();
	}

	private void loadConfiguration()
	{
		try
		{
			ChristmasCraft.instance.forceChristmasTime = configuration.get(Configuration.CATEGORY_GENERAL, "forceChristmasTime", false).getBoolean();
			ChristmasCraft.instance.allowGiftception = configuration.get(Configuration.CATEGORY_GENERAL, "allowGiftsInsideGifts", false).getBoolean();
			ChristmasCraft.instance.playSnowSounds = configuration.get(Configuration.CATEGORY_GENERAL, "playBellsWhenSnowing", true).getBoolean();
			ChristmasCraft.instance.generateSnow = this.configuration.get(Configuration.CATEGORY_GENERAL, "generateSnow", true).getBoolean();
		}
		catch (Exception e)
		{
			ChristmasCraft.logger.error("Mod has a problem loading it's configuration", e);
		}
		finally
		{
			if (configuration.hasChanged())
			{
				configuration.save();
			}
		}
	}

	public void save()
	{
		if (configuration.hasChanged())
		{
			configuration.save();
		}
	}

	@SubscribeEvent
	public void onConfigurationChangedEvent(ConfigChangedEvent.OnConfigChangedEvent event)
	{
		if (event.modID.equalsIgnoreCase("ChristmasCraft"))
		{
			loadConfiguration();
		}
	}
}