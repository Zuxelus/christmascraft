package org.newthead.christmas;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemReed;
import net.minecraft.item.ItemStack;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.biome.BiomeGenBeach;
import net.minecraft.world.biome.BiomeGenEnd;
import net.minecraft.world.biome.BiomeGenHell;
import net.minecraft.world.biome.BiomeGenMushroomIsland;
import net.minecraft.world.biome.BiomeGenOcean;
import net.minecraft.world.biome.BiomeGenRiver;
import net.minecraft.world.biome.BiomeGenSwamp;

import org.apache.logging.log4j.Logger;
import org.newthead.christmas.blocks.BlockChristmasLight;
import org.newthead.christmas.blocks.BlockChristmasPresent;
import org.newthead.christmas.blocks.BlockChristmasStar;
import org.newthead.christmas.blocks.BlockChristmasStocking;
import org.newthead.christmas.blocks.BlockFruitcake;
import org.newthead.christmas.blocks.BlockOrnament;
import org.newthead.christmas.blocks.BlockTreestand;
import org.newthead.christmas.blocks.BlockWreath;
import org.newthead.christmas.entities.EntityCreeperClaus;
import org.newthead.christmas.entities.EntityEnderElf;
import org.newthead.christmas.items.ItemChristmasDrink;
import org.newthead.christmas.items.ItemChristmasLight;
import org.newthead.christmas.items.ItemChristmasPresent;
import org.newthead.christmas.items.ItemChristmasRecord;
import org.newthead.christmas.items.ItemChristmasStocking;
import org.newthead.christmas.items.ItemIceSkates;
import org.newthead.christmas.items.ItemOrnament;
import org.newthead.christmas.items.ItemTreestand;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid="ChristmasCraft", name="ChristmasCraft", version="3.0.1", guiFactory = "org.newthead.christmas.GuiFactory")
public class ChristmasCraft
{
	@Mod.Instance("ChristmasCraft")
	public static ChristmasCraft instance;
	public static Logger logger;
	public static ConfigurationHandler config;
	public static boolean forceChristmasTime = false;
	public static boolean allowGiftception = false;
	public static boolean playSnowSounds = true;
	public static boolean generateSnow = true;
	private static Calendar calendar = Calendar.getInstance();
	public static final String RESOURCE_BASE = "christmascraft";
	@SidedProxy(clientSide="org.newthead.christmas.ClientModSetup", serverSide="org.newthead.christmas.ModSetup")
	public static ModSetup setup;
	public static Block blockChristmasLightOff;
	public static Block blockChristmasLightOn;
	public static Block blockWreath;
	public static Block blockFruitcake;
	public static Block blockStocking;
	public static Block blockOrnament;
	public static Block blockTreestand;
	public static Block blockPresent;
	public static Block blockChristmasStar;
	public static Item itemChristmasSpice;
	public static Item itemChocolateMilk;
	public static Item itemHotCocoa;
	public static Item itemCandycane;
	public static Item itemGingerbread;
	public static Item itemEggnog;
	public static Item itemFruitcake;
	public static Item itemIceSkates;
	public static Item recordWish;
	public static Item recordCarol;
	public static Item recordJingle;
	private static Random random = new Random();
	public static ItemStack[] stockingPresents;
	public static ItemStack[] treePresents;

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		logger = event.getModLog();
		//Load configuration
		config = new ConfigurationHandler();
		FMLCommonHandler.instance().bus().register(config);
		config.init(event.getSuggestedConfigurationFile());
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent event)
	{
		createBlocks();

		GameRegistry.registerBlock(blockChristmasLightOff, ItemChristmasLight.class, "blockChristmasLightOff");
		GameRegistry.registerBlock(blockChristmasLightOn, ItemChristmasLight.class, "blockChristmasLightOn");
		GameRegistry.registerBlock(blockWreath, "blockWreath");
		GameRegistry.registerBlock(blockFruitcake, "blockFruitcake");
		GameRegistry.registerBlock(blockStocking, ItemChristmasStocking.class, "blockStocking");
		GameRegistry.registerBlock(blockOrnament, ItemOrnament.class, "blockOrnament");
		GameRegistry.registerBlock(blockTreestand, ItemTreestand.class, "blockTreestand");
		GameRegistry.registerBlock(blockPresent, ItemChristmasPresent.class, "blockPresent");
		GameRegistry.registerBlock(blockChristmasStar, "blockChristmasStar");

		createItems();

		setup.registerTileEntities();
		setup.registerEntities();
		setup.registerCraftingRecipes();
		setup.registerRenderers();
		
		FMLCommonHandler.instance().bus().register(WeatherTickHandler.instance);
		FMLCommonHandler.instance().bus().register(PlayersSleepingHandler.instance);
		if (event.getSide().isClient())
		{
			FMLCommonHandler.instance().bus().register(SnowSoundTickHandler.instance);
		}
	}

	private void createBlocks()
	{
		blockChristmasLightOff = new BlockChristmasLight(false).setHardness(0.1F).setBlockName("xmasLight").setBlockTextureName("christmascraft:xmasLight_off").setCreativeTab(CreativeTabs.tabRedstone);
		blockChristmasLightOn = new BlockChristmasLight(true).setHardness(0.1F).setBlockName("xmasLight").setBlockTextureName("christmascraft:xmasLight_on").setCreativeTab(CreativeTabs.tabRedstone);
		blockWreath = new BlockWreath().setHardness(0.2F).setBlockName("wreath").setBlockTextureName("christmascraft:wreath").setCreativeTab(CreativeTabs.tabDecorations);
		blockFruitcake = new BlockFruitcake().setHardness(0.5F).setStepSound(Block.soundTypeCloth).setBlockName("fruitcake").setBlockTextureName("christmascraft:fruitcake");
		blockStocking = new BlockChristmasStocking().setBlockName("stocking").setBlockTextureName("christmascraft:stocking").setCreativeTab(CreativeTabs.tabDecorations);
		blockOrnament = new BlockOrnament().setHardness(0.1F).setBlockName("ornament").setBlockTextureName("christmascraft:ornament").setCreativeTab(CreativeTabs.tabDecorations);
		blockTreestand = new BlockTreestand().setHardness(2.0F).setResistance(5.0F).setBlockName("treestand").setBlockTextureName("christmascraft:treestand").setCreativeTab(CreativeTabs.tabDecorations);
		blockPresent = new BlockChristmasPresent().setHardness(2.0F).setResistance(3.0F).setBlockName("xmasGift").setBlockTextureName("christmascraft:xmasGift");
		blockChristmasStar = new BlockChristmasStar().setBlockName("xmasStar").setBlockTextureName("christmascraft:xmasStar").setCreativeTab(CreativeTabs.tabDecorations);
	}

	private void createItems()
	{
		itemChristmasSpice = new Item().setUnlocalizedName("xmasSpice").setTextureName("christmascraft:xmasSpice").setCreativeTab(CreativeTabs.tabFood);
		GameRegistry.registerItem(itemChristmasSpice, "xmasSpice");
		itemChocolateMilk = new ItemChristmasDrink("chocolateMilk", 3, 0.3F);
		itemHotCocoa = new ItemChristmasDrink("hotCocoa", 8, 0.8F);
		itemCandycane = new ItemFood(2, 0.6F, false).setUnlocalizedName("candycane").setTextureName("christmascraft:candycane").setCreativeTab(CreativeTabs.tabFood);
		GameRegistry.registerItem(itemCandycane, "candycane");
		itemGingerbread = new ItemFood(1, 0.1F, false).setUnlocalizedName("gingerbread").setTextureName("christmascraft:gingerbread").setCreativeTab(CreativeTabs.tabFood);
		GameRegistry.registerItem(itemGingerbread, "gingerbread");
		itemEggnog = new ItemChristmasDrink("eggnog", 5, 0.6F);
		itemFruitcake = new ItemReed(blockFruitcake).setMaxStackSize(1).setUnlocalizedName("fruitcake").setTextureName("christmascraft:fruitcake").setCreativeTab(CreativeTabs.tabFood);
		GameRegistry.registerItem(itemFruitcake, "fruitcake");
		recordWish = new ItemChristmasRecord("wish");
		recordCarol = new ItemChristmasRecord("carol");
		recordJingle = new ItemChristmasRecord("jingle");
		itemIceSkates = new ItemIceSkates("iceSkates");
	}

	@Mod.EventHandler
	public void postInit(FMLPostInitializationEvent event)
	{
		editWorldGen();
		populatePresentLists();
	}

	private void editWorldGen()
	{
		if (!isChristmasTime())
		{
			return;
		}
		for (int i = 0; i < BiomeGenBase.getBiomeGenArray().length; i++)
		{
			if (BiomeGenBase.getBiome(i) != null)
			{
				BiomeGenBase biome = BiomeGenBase.getBiome(i);
				if (biome.temperature <= 1.0F && !(biome instanceof BiomeGenSwamp) && !(biome instanceof BiomeGenOcean) && !(biome instanceof BiomeGenHell) && !(biome instanceof BiomeGenEnd) && !(biome instanceof BiomeGenMushroomIsland) && !(biome instanceof BiomeGenBeach) && !(biome instanceof BiomeGenRiver))
				{
					if (generateSnow)
					{
						biome.temperature = 0.0F;
						biome.setEnableSnow();
					}
					EntityRegistry.addSpawn(EntityCreeperClaus.class, 5, 1, 1, EnumCreatureType.creature, new BiomeGenBase[] { biome });
					EntityRegistry.addSpawn(EntityEnderElf.class, 1, 1, 4, EnumCreatureType.monster , new BiomeGenBase[] { biome });
					EntityRegistry.removeSpawn(EntityEnderman.class, EnumCreatureType.monster , new BiomeGenBase[] { biome });
				}
			}
		}
	}

	public static boolean isChristmasTime()
	{
		if (forceChristmasTime)
		{
			return true;
		}
		calendar.setTime(new Date());
		if (calendar.get(2) == 11)
		{
			return true;
		}
		return false;
	}

	public static boolean isChristmasDay()
	{
		calendar.setTime(new Date());
		return (calendar.get(2) == 11) && (calendar.get(5) == 25);
	}

	public static boolean canWrapGiftsInsideGifts()
	{
		return allowGiftception;
	}

	public static boolean shouldPlaySnowSound()
	{
		return playSnowSounds;
	}

	public static ItemStack getRandomStockingGift()
	{
		return PresentRecipe.createPresentFromItemStack(stockingPresents[random.nextInt(stockingPresents.length)]);
	}

	public static ItemStack getRandomTreeGift()
	{
		return PresentRecipe.createPresentFromItemStack(treePresents[random.nextInt(treePresents.length)]);
	}

	private void populatePresentLists()
	{
		stockingPresents = new ItemStack[] { new ItemStack(Items.apple), new ItemStack(Items.boat), new ItemStack(Items.book), new ItemStack(Items.bread), new ItemStack(Items.coal), new ItemStack(Items.compass), new ItemStack(Items.fishing_rod), new ItemStack(Items.painting), new ItemStack(Items.clock), new ItemStack(recordWish), new ItemStack(recordCarol), new ItemStack(recordJingle), new ItemStack(Items.saddle), new ItemStack(Items.slime_ball), new ItemStack(Items.speckled_melon), new ItemStack(itemCandycane), new ItemStack(itemGingerbread), new ItemStack(Items.iron_axe), new ItemStack(Items.iron_boots), new ItemStack(Items.iron_helmet), new ItemStack(Items.iron_hoe), new ItemStack(Items.iron_leggings), new ItemStack(Items.iron_pickaxe), new ItemStack(Items.iron_chestplate), new ItemStack(Items.iron_shovel), new ItemStack(Items.iron_sword), new ItemStack(Items.emerald), new ItemStack(Items.golden_carrot) };
		treePresents = new ItemStack[] { new ItemStack(Items.golden_apple), new ItemStack(Items.diamond_axe), new ItemStack(Items.diamond_boots), new ItemStack(Items.diamond), new ItemStack(Items.diamond_helmet), new ItemStack(Items.diamond_hoe), new ItemStack(Items.diamond_leggings), new ItemStack(Items.diamond_pickaxe), new ItemStack(Items.diamond_chestplate), new ItemStack(Items.diamond_shovel), new ItemStack(Items.diamond_sword), new ItemStack(Items.speckled_melon), new ItemStack(Blocks.sticky_piston), new ItemStack(Blocks.gold_block), new ItemStack(Blocks.iron_block), new ItemStack(Blocks.enchanting_table), new ItemStack(Items.brewing_stand), new ItemStack(Items.blaze_rod), new ItemStack(Items.ender_eye), new ItemStack(blockFruitcake), new ItemStack(itemIceSkates) };
	}
}
