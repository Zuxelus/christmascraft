package org.newthead.christmas;

import net.minecraftforge.common.MinecraftForge;

import org.newthead.christmas.entities.EntityCreeperClaus;
import org.newthead.christmas.entities.EntityEnderElf;
import org.newthead.christmas.renderers.RenderChristmasLight;
import org.newthead.christmas.renderers.RenderCreeperClaus;
import org.newthead.christmas.renderers.RenderEnderElf;
import org.newthead.christmas.renderers.RenderIceSkates;
import org.newthead.christmas.renderers.RenderOrnament;
import org.newthead.christmas.renderers.RenderPresent;
import org.newthead.christmas.renderers.RenderStocking;
import org.newthead.christmas.renderers.RenderTreestand;
import org.newthead.christmas.renderers.RenderWreath;

import cpw.mods.fml.client.registry.RenderingRegistry;

public class ClientModSetup extends ModSetup
{
	public void registerRenderers()
	{
		super.registerRenderers();
		RenderingRegistry.registerBlockHandler(new RenderWreath());
		RenderingRegistry.registerBlockHandler(new RenderChristmasLight());
		RenderingRegistry.registerBlockHandler(new RenderOrnament());
		RenderingRegistry.registerBlockHandler(new RenderTreestand());
		RenderingRegistry.registerBlockHandler(new RenderPresent());
		RenderingRegistry.registerBlockHandler(new RenderStocking());

		RenderingRegistry.registerEntityRenderingHandler(EntityCreeperClaus.class, new RenderCreeperClaus());
		RenderingRegistry.registerEntityRenderingHandler(EntityEnderElf.class, new RenderEnderElf());

		MinecraftForge.EVENT_BUS.register(new RenderIceSkates());
	}

	@Override
	public void registerTileEntities()
	{
		super.registerTileEntities();
	}

	@Override
	public void registerEntities()
	{
		super.registerEntities();
	}

	@Override
	public void registerCraftingRecipes()
	{
		super.registerCraftingRecipes();
	}
}
