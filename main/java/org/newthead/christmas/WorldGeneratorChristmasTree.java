package org.newthead.christmas;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;

public class WorldGeneratorChristmasTree extends WorldGenerator
{
	public WorldGeneratorChristmasTree(boolean notify)
	{
		super(notify);
	}

	@Override
	public boolean generate(World world, Random random, int x, int y, int z)
	{
		int height = 3;
		boolean hasRoom = true;
		if ((y < 1) || (y + height + 1 >= world.getHeight()))
		{
			return false;
		}
		for (int yOffset = 0; (yOffset < height) && (hasRoom); yOffset++)
		{
			int yLoc = y + yOffset;
			switch (yOffset)
			{
			case 0: 
				for (int xOffset = -1; (xOffset <= 1) && (hasRoom); xOffset++)
				{
					for (int zOffset = -1; (zOffset <= 1) && (hasRoom); zOffset++)
					{
						hasRoom = canGrowAtLocation(world, x + xOffset, yLoc, z + zOffset);
					}
				}
				break;
			case 1: 
				for (int xOffset = -1; (xOffset <= 1) && (hasRoom); xOffset++)
				{
					for (int zOffset = -1; (zOffset <= 1) && (hasRoom); zOffset++)
					{
						if (Math.abs(xOffset) + Math.abs(zOffset) <= 1)
						{
							hasRoom = canGrowAtLocation(world, x + xOffset, yLoc, z + zOffset);
						}
					}
				}
				break;
			case 2: 
				hasRoom = canGrowAtLocation(world, x, yLoc, z);
			}
		}
		if (!hasRoom)
		{
			return false;
		}
		if (world.getBlock(x, y - 1, z) != ChristmasCraft.blockTreestand || y >= world.getHeight() - height - 1)
		{
			return false;
		}
		for (int yOffset = 0; yOffset < height; yOffset++)
		{
			int yLoc = y + yOffset;
			switch (yOffset)
			{
			case 0: 
				for (int xOffset = -1; (xOffset <= 1) && (hasRoom); xOffset++)
				{
					for (int zOffset = -1; (zOffset <= 1) && (hasRoom); zOffset++)
					{
						if ((xOffset == 0) && (zOffset == 0))
						{
							setBlockAndNotifyAdequately(world, x + xOffset, yLoc, z + zOffset, Blocks.log, 1);
						}
						else
						{
							setBlockAndNotifyAdequately(world, x + xOffset, yLoc, z + zOffset, Blocks.leaves, 1);
						}
					}
				}
				break;
			case 1: 
				for (int xOffset = -1; (xOffset <= 1) && (hasRoom); xOffset++)
				{
					for (int zOffset = -1; (zOffset <= 1) && (hasRoom); zOffset++)
					{
						if (Math.abs(xOffset) + Math.abs(zOffset) <= 1) 
						{
							if ((xOffset == 0) && (zOffset == 0))
							{
								setBlockAndNotifyAdequately(world, x + xOffset, yLoc, z + zOffset, Blocks.log, 1);
							}
							else
							{
								setBlockAndNotifyAdequately(world, x + xOffset, yLoc, z + zOffset, Blocks.leaves, 1);
							}
						}
					}
				}
				break;
			case 2: 
				setBlockAndNotifyAdequately(world, x, yLoc, z, Blocks.leaves, 1);
			}
		}
		return true;
	}

	private boolean canGrowAtLocation(World world, int x, int y, int z)
	{
		Block block = world.getBlock(x, y, z);
		return block != null || block == Blocks.leaves;
	}
}
