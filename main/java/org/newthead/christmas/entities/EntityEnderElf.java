package org.newthead.christmas.entities;

import java.util.UUID;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.util.EntityDamageSourceIndirect;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.EnderTeleportEvent;

import org.newthead.christmas.ChristmasCraft;
import org.newthead.christmas.tileentities.TileEntityChristmasPresent;

public class EntityEnderElf extends EntityMob
{
	private static final UUID attackingSpeedBoostModifierUUID = UUID.fromString("020E0DFB-87AE-4653-9556-831010E291A0");
	private static final AttributeModifier attackingSpeedBoostModifier = (new AttributeModifier(attackingSpeedBoostModifierUUID, "Attacking speed boost", 6.199999809265137D, 0)).setSaved(false);
	private int teleportDelay;
	private int stareTimer;
	private Entity lastEntityToAttack;
	private boolean isAggressive;

	public EntityEnderElf(World world)
	{
		super(world);
		setSize(0.6F, 2.9F);
		this.stepHeight = 1.0F;
	}

	protected void applyEntityAttributes()
	{
		super.applyEntityAttributes();
		getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(40.0D);
		getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.30000001192092896D);
		getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(7.0D);
	}

	@Override
	protected void entityInit()
	{
		super.entityInit();
		this.dataWatcher.addObject(16, new Byte((byte)0));
		this.dataWatcher.addObject(17, new Byte((byte)0));
		this.dataWatcher.addObject(18, new Byte((byte)0));
		this.dataWatcher.addObjectByDataType(19, 5);
	}

	@Override
	public void writeEntityToNBT(NBTTagCompound nbttagcompound)
	{
		super.writeEntityToNBT(nbttagcompound);
		nbttagcompound.setShort("carried", (short)Block.getIdFromBlock(getCarried()));
		nbttagcompound.setShort("carriedData", (short)getCarryingData());
		NBTTagCompound presentContents = new NBTTagCompound();
		if (getPresentContents() != null)
		{
			getPresentContents().writeToNBT(presentContents);
			nbttagcompound.setTag("contents", presentContents);
		}
	}

	@Override
	public void readEntityFromNBT(NBTTagCompound nbttagcompound)
	{
		super.readEntityFromNBT(nbttagcompound);
		setCarried(Block.getBlockById(nbttagcompound.getShort("carried")));
		setCarryingData(nbttagcompound.getShort("carriedData"));
		NBTTagCompound presentContents = nbttagcompound.getCompoundTag("contents");
		if (presentContents != null)
		{
			setPresentContents(ItemStack.loadItemStackFromNBT(presentContents));
		}
	}

	@Override
	protected Entity findPlayerToAttack()
	{
		EntityPlayer entityplayer = this.worldObj.getClosestVulnerablePlayerToEntity(this, 64.0D);
		if (entityplayer != null)
		{
			if (shouldAttackPlayer(entityplayer))
			{
				this.isAggressive = true;
				if (this.stareTimer == 0)
				{
					this.worldObj.playSoundEffect(entityplayer.posX, entityplayer.posY, entityplayer.posZ, "mob.endermen.stare", 1.0F, 1.0F);
				}
				if (this.stareTimer++ == 5)
				{
					this.stareTimer = 0;
					setScreaming(true);
					return entityplayer;
				}
			}
			else
			{
				this.stareTimer = 0;
			}
		}
		return null;
	}

	private boolean shouldAttackPlayer(EntityPlayer par1EntityPlayer)
	{
		ItemStack itemstack = par1EntityPlayer.inventory.armorInventory[3];
		if (itemstack != null && itemstack.getItem() == Item.getItemFromBlock(Blocks.pumpkin))
		{
			return false;
		}
		Vec3 vec3 = par1EntityPlayer.getLook(1.0F).normalize();
		Vec3 vec31 = Vec3.createVectorHelper(this.posX - par1EntityPlayer.posX, this.boundingBox.minY + (double)(this.height / 2.0F) - (par1EntityPlayer.posY + (double)par1EntityPlayer.getEyeHeight()), this.posZ - par1EntityPlayer.posZ);
		double d0 = vec31.lengthVector();
		vec31 = vec31.normalize();
		double d1 = vec3.dotProduct(vec31);
		return d1 > 1.0D - 0.025D / d0 && par1EntityPlayer.canEntityBeSeen(this);
	}

	@Override
	public void onLivingUpdate()
	{
		if (isWet())
		{
			attackEntityFrom(DamageSource.drown, 1.0F);
		}
		if (this.lastEntityToAttack != this.entityToAttack)
		{
			IAttributeInstance iattributeinstance = getEntityAttribute(SharedMonsterAttributes.movementSpeed);
			iattributeinstance.removeModifier(attackingSpeedBoostModifier);
			if (this.entityToAttack != null)
			{
				iattributeinstance.applyModifier(attackingSpeedBoostModifier);
			}
		}
		this.lastEntityToAttack = this.entityToAttack;
		if (!this.worldObj.isRemote && this.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing"))
		{
			if (getCarried().getMaterial() == Material.air)
			{
				if (this.rand.nextInt(600) == 0)
				{
					ItemStack present = ChristmasCraft.getRandomTreeGift();
					if (present != null)
					{
						int metadata = present.getItemDamage() % 4;
						setCarried(ChristmasCraft.blockPresent);
						setCarryingData(metadata);
						setPresentContents(present);
					}
				}
			}
			else if (this.rand.nextInt(2000) == 0)
			{
				int j = MathHelper.floor_double(this.posX - 1.0D + this.rand.nextDouble() * 2.0D);
				int i1 = MathHelper.floor_double(this.posY + this.rand.nextDouble() * 2.0D);
				int k1 = MathHelper.floor_double(this.posZ - 1.0D + this.rand.nextDouble() * 2.0D);
				Block block = this.worldObj.getBlock(j, i1, k1);
				Block block1 = this.worldObj.getBlock(j, i1 - 1, k1);
				if (block.getMaterial() == Material.air && block1.getMaterial() != Material.air  && ChristmasCraft.blockPresent.canPlaceBlockAt(this.worldObj, j, i1, k1))
				{
					setCarried(Blocks.air);
					this.worldObj.setBlock(j, i1, k1, ChristmasCraft.blockPresent, getCarryingData(), 3);
					TileEntityChristmasPresent tile = (TileEntityChristmasPresent)this.worldObj.getTileEntity(j, i1, k1);
					if (tile != null)
					{
						ItemStack itemstack = getPresentContents();
						tile.setContents(itemstack);
					}
				}
			}
		}
		for (int i = 0; i < 2; i++)
		{
			this.worldObj.spawnParticle("portal", this.posX + (this.rand.nextDouble() - 0.5D) * (double)this.width, this.posY + this.rand.nextDouble() * (double)this.height - 0.25D, this.posZ + (this.rand.nextDouble() - 0.5D) * (double)this.width, (this.rand.nextDouble() - 0.5D) * 2.0D, -this.rand.nextDouble(), (this.rand.nextDouble() - 0.5D) * 2.0D);
		}
		if (this.worldObj.isDaytime() && !this.worldObj.isRemote)
		{
			float var6 = getBrightness(1.0F);
			if (var6 > 0.5F && this.worldObj.canBlockSeeTheSky(MathHelper.floor_double(this.posX), MathHelper.floor_double(this.posY), MathHelper.floor_double(this.posZ)) && this.rand.nextFloat() * 30.0F < (var6 - 0.4F) * 2.0F)
			{
				this.entityToAttack = null;
				setScreaming(false);
				teleportRandomly();
			}
		}
		if (isWet() || isBurning())
		{
			this.entityToAttack = null;
			setScreaming(false);
			this.isAggressive = false;
			teleportRandomly();
		}
		if (isScreaming() && !this.isAggressive && this.rand.nextInt(100) == 0)
		{
			setScreaming(false);
		}
		this.isJumping = false;
		if (this.entityToAttack != null)
		{
			faceEntity(this.entityToAttack, 100.0F, 100.0F);
		}
		if (!this.worldObj.isRemote && isEntityAlive())
		{
			if (this.entityToAttack != null)
			{
				if (this.entityToAttack instanceof EntityPlayer && shouldAttackPlayer((EntityPlayer)this.entityToAttack))
				{
					if (this.entityToAttack.getDistanceSqToEntity(this) < 16.0D)
					{
						teleportRandomly();
					}
					this.teleportDelay = 0;
				}
				else if (this.entityToAttack.getDistanceSqToEntity(this) > 256.0D && this.teleportDelay++ >= 30 && teleportToEntity(this.entityToAttack))
				{
					this.teleportDelay = 0;
				}
			}
			else
			{
				setScreaming(false);
				this.teleportDelay = 0;
			}
		}
		super.onLivingUpdate();
	}

	protected boolean teleportRandomly()
	{
		double var1 = this.posX + (this.rand.nextDouble() - 0.5D) * 64.0D;
		double var3 = this.posY + (double)(this.rand.nextInt(64) - 32);
		double var5 = this.posZ + (this.rand.nextDouble() - 0.5D) * 64.0D;
		return teleportTo(var1, var3, var5);
	}

	protected boolean teleportToEntity(Entity par1Entity)
	{
		Vec3 var2 = Vec3.createVectorHelper(this.posX - par1Entity.posX, this.boundingBox.minY + this.height / 2.0F - par1Entity.posY + par1Entity.getEyeHeight(), this.posZ - par1Entity.posZ);
		var2 = var2.normalize();
		double var3 = 16.0D;
		double var5 = this.posX + (this.rand.nextDouble() - 0.5D) * 8.0D - var2.xCoord * var3;
		double var7 = this.posY + (double)(this.rand.nextInt(16) - 8) - var2.yCoord * var3;
		double var9 = this.posZ + (this.rand.nextDouble() - 0.5D) * 8.0D - var2.zCoord * var3;
		return teleportTo(var5, var7, var9);
	}

	protected boolean teleportTo(double par1, double par3, double par5)
	{
		EnderTeleportEvent event = new EnderTeleportEvent(this, par1, par3, par5, 0.0F);
		if (MinecraftForge.EVENT_BUS.post(event))
		{
			return false;
		}
		double d3 = this.posX;
		double d4 = this.posY;
		double d5 = this.posZ;
		this.posX = event.targetX;
		this.posY = event.targetY;
		this.posZ = event.targetZ;
		boolean flag = false;
		int i = MathHelper.floor_double(this.posX);
		int j = MathHelper.floor_double(this.posY);
		int k = MathHelper.floor_double(this.posZ);
		if (this.worldObj.blockExists(i, j, k))
		{
			boolean flag1 = false;
			while (!flag1 && j > 0)
			{
				Block block = this.worldObj.getBlock(i, j - 1, k);

				if (block.getMaterial().blocksMovement())

				{
					flag1 = true;
				}
				else
				{
					this.posY -= 1.0D;
					j--;
				}
			}
			if (flag1)
			{
				setPosition(this.posX, this.posY, this.posZ);
				if (this.worldObj.getCollidingBoundingBoxes(this, this.boundingBox).isEmpty() && !this.worldObj.isAnyLiquid(this.boundingBox))
				{
					flag = true;
				}
			}
		}
		if (!flag)
		{
			setPosition(d3, d4, d5);
			return false;
		}
		short short1 = 128;
		for (int l = 0; l < short1; l++)
		{
			double d6 = (double)l / ((double)short1 - 1.0D);
			float f = (this.rand.nextFloat() - 0.5F) * 0.2F;
			float f1 = (this.rand.nextFloat() - 0.5F) * 0.2F;
			float f2 = (this.rand.nextFloat() - 0.5F) * 0.2F;
			double d7 = d3 + (this.posX - d3) * d6 + (this.rand.nextDouble() - 0.5D) * (double)this.width * 2.0D;
			double d8 = d4 + (this.posY - d4) * d6 + this.rand.nextDouble() * (double)this.height;
			double d9 = d5 + (this.posZ - d5) * d6 + (this.rand.nextDouble() - 0.5D) * (double)this.width * 2.0D;
			this.worldObj.spawnParticle("portal", d7, d8, d9, (double)f, (double)f1, (double)f2);
		}
		this.worldObj.playSoundEffect(d3, d4, d5, "mob.endermen.portal", 1.0F, 1.0F);
		playSound("mob.endermen.portal", 1.0F, 1.0F);
		return true;
	}

	@Override
	protected String getLivingSound()
	{
		return isScreaming() ? "mob.endermen.scream" : "mob.endermen.idle";
	}

	@Override
	protected String getHurtSound()
	{
		return "mob.endermen.hit";
	}

	@Override
	protected String getDeathSound()
	{
		return "mob.endermen.death";
	}

	@Override
	protected Item getDropItem()
	{
		return Item.getItemFromBlock(ChristmasCraft.blockPresent);
	}

	@Override
	protected void dropFewItems(boolean par1, int par2)
	{
		ItemStack present = getPresentContents();
		if (getCarried() != null && present != null) //?
		{
			entityDropItem(present, 0.0F);
		}
	}

	public void setCarried(Block par1)
	{
		this.dataWatcher.updateObject(16, Byte.valueOf((byte)(Block.getIdFromBlock(par1) & 0xFF)));
	}

	public Block getCarried()
	{
		return Block.getBlockById(this.dataWatcher.getWatchableObjectByte(16));
	}

	public void setCarryingData(int par1)
	{
		this.dataWatcher.updateObject(17, Byte.valueOf((byte)(par1 & 0xFF)));
	}

	public int getCarryingData()
	{
		return this.dataWatcher.getWatchableObjectByte(17);
	}

	public void setPresentContents(ItemStack i)
	{
		if (i != null)
		{
			this.dataWatcher.updateObject(19, i);
		}
	}

	public ItemStack getPresentContents()
	{
		return this.dataWatcher.getWatchableObjectItemStack(19);
	}

	public boolean attackEntityFrom(DamageSource par1DamageSource, float par2)
	{
		if (isEntityInvulnerable())
		{
			return false;
		}
		setScreaming(true);
		if (par1DamageSource instanceof EntityDamageSource && par1DamageSource.getEntity() instanceof EntityPlayer)
		{
			this.isAggressive = true;
		}
		if (par1DamageSource instanceof EntityDamageSourceIndirect)
		{
			this.isAggressive = false;
			for (int i = 0; i < 64; i++)
			{
				if (teleportRandomly())
				{
					return true;
				}
			}
			return super.attackEntityFrom(par1DamageSource, par2);
		}
		return super.attackEntityFrom(par1DamageSource, par2);
	}

	public boolean isScreaming()
	{
		return this.dataWatcher.getWatchableObjectByte(18) > 0;
	}

	public void setScreaming(boolean par1)
	{
		this.dataWatcher.updateObject(18, Byte.valueOf((byte)(par1 ? 1 : 0)));
	}
}
