package org.newthead.christmas.entities;

import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAIPanic;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

import org.newthead.christmas.ChristmasCraft;

public class EntityCreeperClaus extends EntityAnimal
{
	public EntityCreeperClaus(World world)
	{
		super(world);
		setSize(0.8F, 1.6F);
		getNavigator().setAvoidsWater(true);
		this.tasks.addTask(1, new EntityAIPanic(this, 2.0D));
		this.tasks.addTask(5, new EntityAIWander(this, 1.0D));
		this.tasks.addTask(6, new EntityAIWatchClosest(this, EntityPlayer.class, 6.0F));
		this.tasks.addTask(7, new EntityAILookIdle(this));
	}

	@Override
	public void writeEntityToNBT(NBTTagCompound nbttagcompound)
	{
		super.writeEntityToNBT(nbttagcompound);
	}

	@Override
	public void readEntityFromNBT(NBTTagCompound nbttagcompound)
	{
		super.readEntityFromNBT(nbttagcompound);
	}

	@Override
	protected String getHurtSound()
	{
		return "mob.creeper";
	}

	@Override
	protected String getDeathSound()
	{
		return "mob.creeperdeath";
	}

	@Override
	protected Item getDropItem()
	{
		return ChristmasCraft.itemChristmasSpice;
	}

	@Override
	protected void dropFewItems(boolean flag, int i)
	{
		Item item = this.getDropItem();

		if (item != null)
		{
			int j = this.rand.nextInt(2 + i);

			for (int k = 0; k < j; ++k)
			{
				this.dropItem(item, 1);
			}
		}
	}

	@Override
	public EntityAgeable createChild(EntityAgeable var1)
	{
		return null;
	}

	@Override
	public boolean isBreedingItem(ItemStack par1ItemStack)
	{
		return false;
	}
}
