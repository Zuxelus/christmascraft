package org.newthead.christmas;

import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.common.config.Configuration;
import cpw.mods.fml.client.config.GuiConfig;

public class ConfigGui extends GuiConfig 
{
    public ConfigGui(GuiScreen parent) 
    {
        super(parent, new ConfigElement(ChristmasCraft.config.configuration.getCategory(Configuration.CATEGORY_GENERAL)).getChildElements(), 
        		"ChristmasCraft", false, false, GuiConfig.getAbridgedConfigPath(ChristmasCraft.config.configuration.toString()));
    }
}
