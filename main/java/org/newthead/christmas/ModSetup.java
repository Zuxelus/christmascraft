package org.newthead.christmas;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

import org.newthead.christmas.blocks.BlockChristmasLight;
import org.newthead.christmas.blocks.BlockOrnament;
import org.newthead.christmas.entities.EntityCreeperClaus;
import org.newthead.christmas.entities.EntityEnderElf;
import org.newthead.christmas.tileentities.TileEntityChristmasPresent;
import org.newthead.christmas.tileentities.TileEntityStocking;
import org.newthead.christmas.tileentities.TileEntityTreestand;

import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;

public class ModSetup
{
	public static int christmasLightRenderId;
	public static int wreathRenderId;
	public static int stockingRenderId;
	public static int ornamentRenderId;
	public static int treestandRenderId;
	public static int presentRenderId;

	public void registerRenderers() {}

	public void registerTileEntities()
	{
		GameRegistry.registerTileEntity(TileEntityStocking.class, "Stocking");
		GameRegistry.registerTileEntity(TileEntityChristmasPresent.class, "Present");
		GameRegistry.registerTileEntity(TileEntityTreestand.class, "XmasTreeStand");
	}

	public void registerEntities()
	{
		int creeperClausID = EntityRegistry.findGlobalUniqueEntityId();
		EntityRegistry.registerGlobalEntityID(EntityCreeperClaus.class, "creeperclaus", creeperClausID, 894731, 16724736);

		int enderElfID = EntityRegistry.findGlobalUniqueEntityId();
		EntityRegistry.registerGlobalEntityID(EntityEnderElf.class, "enderelf", enderElfID, 1722134, 14277081);
	}

	public void registerCraftingRecipes()
	{
		int[] christmasLightDyeColors = BlockChristmasLight.dyeColors;
		for (int meta = 0; meta < christmasLightDyeColors.length; meta++)
		{
			int dyeMeta = christmasLightDyeColors[meta];
			GameRegistry.addRecipe(new ItemStack(ChristmasCraft.blockChristmasLightOff, 8, meta), new Object[] { "#", "X", "Y", Character.valueOf('#'), Items.redstone, Character.valueOf('X'), Blocks.glass, Character.valueOf('Y'), new ItemStack(Items.dye, 1, dyeMeta) });
		}
		GameRegistry.addRecipe(new ItemStack(ChristmasCraft.blockChristmasLightOff, 8, christmasLightDyeColors.length), new Object[] { "#", "X", "Y", Character.valueOf('#'), Items.redstone, Character.valueOf('X'), Blocks.glass, Character.valueOf('Y'), Items.glowstone_dust });
		for (int leafMeta = 0; leafMeta < 4; leafMeta++) {
			GameRegistry.addRecipe(new ItemStack(ChristmasCraft.blockWreath, 1), new Object[] { "###", "#X#", "###", Character.valueOf('#'), new ItemStack(Blocks.leaves, 1, leafMeta), Character.valueOf('X'), new ItemStack(Blocks.wool, 1, 14) });
		}
		int[] ornamentDyeColors = BlockOrnament.dyeColors;
		for (int meta = 0; meta < ornamentDyeColors.length; meta++)
		{
			int dyeMeta = ornamentDyeColors[meta];
			GameRegistry.addRecipe(new ItemStack(ChristmasCraft.blockOrnament, 4, meta), new Object[] { "#", "X", "Y", Character.valueOf('#'), Items.glowstone_dust, Character.valueOf('X'), Blocks.glass, Character.valueOf('Y'), new ItemStack(Items.dye, 1, dyeMeta) });
		}
		GameRegistry.addRecipe(new ItemStack(ChristmasCraft.blockTreestand, 1), new Object[] { "#X#", "YZY", "W W", Character.valueOf('#'), new ItemStack(Items.dye, 1, 2), Character.valueOf('X'), new ItemStack(Blocks.sapling, 1, 1), Character.valueOf('Y'), Blocks.planks, Character.valueOf('Z'), Blocks.dirt, Character.valueOf('W'), new ItemStack(Blocks.wool, 1, 14) });
		GameRegistry.addRecipe(new PresentRecipe());
		GameRegistry.addRecipe(new PresentUnwrapRecipe());
		GameRegistry.addRecipe(new ItemStack(ChristmasCraft.blockChristmasStar, 1), new Object[] { " # ", "#X#", "# #", Character.valueOf('#'), Items.gold_nugget, Character.valueOf('X'), Items.glowstone_dust });
		GameRegistry.addShapelessRecipe(new ItemStack(ChristmasCraft.itemChocolateMilk), new Object[] { new ItemStack(Items.dye, 1, 3), Items.milk_bucket });
		for (int leafMeta = 0; leafMeta < 4; leafMeta++)
		{
			ItemStack leafType = new ItemStack(Blocks.leaves, 1, leafMeta);
			GameRegistry.addRecipe(new ItemStack(ChristmasCraft.blockWreath, 1), new Object[] { "###", "#X#", "###", Character.valueOf('#'), leafType, Character.valueOf('X'), new ItemStack(Blocks.wool, 1, 14) });
		}
		GameRegistry.addRecipe(new ItemStack(ChristmasCraft.itemCandycane, 2), new Object[] { "X#", " #", " #", Character.valueOf('X'), new ItemStack(Items.dye, 1, 1), Character.valueOf('#'), Items.sugar });
		GameRegistry.addRecipe(new ItemStack(ChristmasCraft.itemGingerbread, 4), new Object[] { "#X#", Character.valueOf('#'), Items.wheat, Character.valueOf('X'), ChristmasCraft.itemChristmasSpice });
		GameRegistry.addShapelessRecipe(new ItemStack(ChristmasCraft.itemEggnog, 1), new Object[] { Items.milk_bucket, Items.egg, ChristmasCraft.itemChristmasSpice });
		GameRegistry.addRecipe(new ItemStack(ChristmasCraft.itemFruitcake, 1), new Object[] { "#S#", "XYX", "WWW", Character.valueOf('#'), Items.milk_bucket, Character.valueOf('S'), ChristmasCraft.itemChristmasSpice, Character.valueOf('X'), Items.melon, Character.valueOf('Y'), Items.egg, Character.valueOf('W'), Items.wheat });
		GameRegistry.addRecipe(new ItemStack(ChristmasCraft.blockStocking, 1), new Object[] { " #X", " Y ", "YY ", Character.valueOf('#'), new ItemStack(Blocks.wool, 1, 0), Character.valueOf('X'), Items.string, Character.valueOf('Y'), new ItemStack(Blocks.wool, 1, 14) });
		GameRegistry.addRecipe(new ItemStack(ChristmasCraft.itemIceSkates, 1), new Object[] { "XYX", "X X", "Z Z", Character.valueOf('X'), Items.leather, Character.valueOf('Y'), Items.string, Character.valueOf('Z'), Items.iron_ingot });
		GameRegistry.addSmelting(ChristmasCraft.itemChocolateMilk, new ItemStack(ChristmasCraft.itemHotCocoa), 0.2F);
	}
}
