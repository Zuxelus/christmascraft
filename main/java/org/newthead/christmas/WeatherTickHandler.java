package org.newthead.christmas;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.ChunkCoordIntPair;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.storage.WorldInfo;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent.Phase;
import cpw.mods.fml.common.gameevent.TickEvent.WorldTickEvent;

public class WeatherTickHandler
{
	public final static WeatherTickHandler instance = new WeatherTickHandler();
	private Random random = new Random();

	@SubscribeEvent
	public void onWorldTick(WorldTickEvent event)
	{
		if (event.phase == Phase.START)
		{
			if (ChristmasCraft.isChristmasTime())
			{
				WorldInfo info = event.world.getWorldInfo();
				if (!info.isRaining() && info.getRainTime() > 24000)
				{
					info.setRainTime(this.random.nextInt(8000) + 16000);
				}
			}
			else
			{
				try
				{
					Field chunkSet = event.world.getClass().getDeclaredField("activeChunkSet");
					chunkSet.setAccessible(true);
					Set activeChunkSet = (Set)chunkSet.get(event.world);

					if (!activeChunkSet.isEmpty())
					{
						int randomChunk = this.random.nextInt(activeChunkSet.size());
						Iterator chunkIter = activeChunkSet.iterator();

						int index = 0;
						ChunkCoordIntPair chunkCoords = null;
						while (chunkIter.hasNext())
						{
							chunkCoords = (ChunkCoordIntPair)chunkIter.next();
							if (index == randomChunk)
							{
								break;
							}
							index++;
						}
						if (chunkCoords != null)
						{
							int offsetX = this.random.nextInt(16);
							int offsetZ = this.random.nextInt(16);
							int worldX = chunkCoords.chunkXPos * 16 + offsetX;
							int worldZ = chunkCoords.chunkZPos * 16 + offsetZ;
							int snowHeight = event.world.getPrecipitationHeight(worldX, worldZ);
							Block block = event.world.getBlock(worldX, snowHeight, worldZ);
							if (block == Blocks.snow)
							{
								BiomeGenBase biome = event.world.getBiomeGenForCoords(worldX, worldZ);
								if (biome.getFloatTemperature(worldX, snowHeight, worldZ) > 0.15F)
								{
									event.world.setBlockToAir(worldX, snowHeight, worldZ);
								}
							}
							else
							{
								block = event.world.getBlock(worldX, snowHeight - 1, worldZ);
								if (block == Blocks.ice)
								{
									BiomeGenBase biome = event.world.getBiomeGenForCoords(worldX, worldZ);
									if (biome.getFloatTemperature(worldX, snowHeight, worldZ) > 0.15F)
									{
										event.world.setBlock(worldX, snowHeight - 1, worldZ, Blocks.water);
									}
								}
							}
						}
					}
				}
				catch (NoSuchFieldException e) { e.printStackTrace(); }
				catch (SecurityException e) { e.printStackTrace(); }
				catch (IllegalArgumentException e) { e.printStackTrace(); } 
				catch (IllegalAccessException e) { e.printStackTrace(); }
			}
		}
	}
}
