package org.newthead.christmas;

import net.minecraft.init.Items;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class PresentRecipe implements IRecipe
{
	private ItemStack recipeOutput = new ItemStack(Item.getItemFromBlock(ChristmasCraft.blockPresent));

	@Override
	public boolean matches(InventoryCrafting var1, World world)
	{
		for (int i = 0; i < 9; i++)
		{
			ItemStack slotItem = var1.getStackInSlot(i);
			if (slotItem != null)
			{
				if (i == 4)
				{
					if (slotItem.getItem() == Item.getItemFromBlock(ChristmasCraft.blockPresent) && !ChristmasCraft.canWrapGiftsInsideGifts())
					{
						return false;
					}
				}
				else
				{
					if (i % 2 == 0 && slotItem.getItem() != Items.string) //?
					{
						return false;
					}
					if (i % 2 == 1 && slotItem.getItem() != Items.paper)
					{
						return false;
					}
				}
			}
			else {
				return false;
			}
		}
		return true;
	}

	@Override
	public ItemStack getCraftingResult(InventoryCrafting var1)
	{
		ItemStack itemToWrap = var1.getStackInSlot(4);
		if (itemToWrap != null) {
			return createPresentFromItemStack(itemToWrap);
		}
		return null;
	}

	@Override
	public int getRecipeSize()
	{
		return 9;
	}

	@Override
	public ItemStack getRecipeOutput()
	{
		return this.recipeOutput;
	}

	public static ItemStack createPresentFromItemStack(ItemStack itemToWrap)
	{
		ItemStack present = new ItemStack(Item.getItemFromBlock(ChristmasCraft.blockPresent));
		present.stackTagCompound = new NBTTagCompound();
		NBTTagCompound contents = new NBTTagCompound();
		ItemStack wrappedItem = itemToWrap.copy();
		wrappedItem.stackSize = 1;
		wrappedItem.writeToNBT(contents);
		present.stackTagCompound.setTag("giftcontents", contents);
		present.setItemDamage(Item.getIdFromItem(wrappedItem.getItem()) % 4);
		return present;
	}

	public static ItemStack createPresentFromItem(Item itemToWrap)
	{
		if (itemToWrap == null)
		{
			return null;
		}
		return createPresentFromItemStack(new ItemStack(itemToWrap));
	}

	public static ItemStack createPresentFromItem(int itemToWrap)
	{
		return createPresentFromItem(Item.getItemById(itemToWrap));
	}
}
