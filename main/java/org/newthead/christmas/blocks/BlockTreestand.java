package org.newthead.christmas.blocks;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraftforge.common.util.ForgeDirection;

import org.newthead.christmas.ModSetup;
import org.newthead.christmas.WorldGeneratorChristmasTree;
import org.newthead.christmas.tileentities.TileEntityTreestand;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockTreestand extends BlockContainer
{
	@SideOnly(Side.CLIENT)
	private IIcon sideIcon;
	@SideOnly(Side.CLIENT)
	private IIcon legsIcon;
	@SideOnly(Side.CLIENT)
	private IIcon topIcon;

	public BlockTreestand()
	{
		super(Material.wood);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return new TileEntityTreestand();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister par1IconRegister)
	{
		this.sideIcon = par1IconRegister.registerIcon(getTextureName() + "_side");
		this.legsIcon = par1IconRegister.registerIcon(getTextureName() + "_legs");
		this.topIcon = par1IconRegister.registerIcon(getTextureName() + "_top");
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getItemIconName()
	{
		return getTextureName();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public int getRenderType()
	{
		return ModSetup.treestandRenderId;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public boolean isSideSolid(IBlockAccess world, int x, int y, int z, ForgeDirection side)
	{
		return (side == ForgeDirection.UP) || (side == ForgeDirection.DOWN);
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int i, int j)
	{
		if (j == 2) {
			return this.legsIcon;
		}
		if (i == 1) {
			return this.topIcon;
		}
		return this.sideIcon;
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float hitX, float hitY, float hitZ)
	{
		if (!world.isRemote && side == 1)
		{
			ItemStack heldItem = player.getCurrentEquippedItem();
			if (heldItem != null && heldItem.getItem() == Items.dye && heldItem.getItemDamage() == 15)
			{
				WorldGenerator treeGen = new WorldGeneratorChristmasTree(true);
				if (treeGen.generate(world, null, x, y + 1, z))
				{
					heldItem.stackSize -= 1;
				}
				return true;
			}
		}
		return false;
	}
}
