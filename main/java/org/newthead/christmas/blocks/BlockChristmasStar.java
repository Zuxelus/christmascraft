package org.newthead.christmas.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.particle.EntityAuraFX;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockChristmasStar extends Block
{
	public BlockChristmasStar()
	{
		super(Material.circuits);
		float f = 0.45F;
		setLightLevel(0.5F);
		setBlockBounds(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, f * 2.0F, 0.5F + f);
	}

	@Override
	public boolean canPlaceBlockAt(World world, int x, int y, int z)
	{
		if (super.canPlaceBlockAt(world, x, y, z))
		{
			Block belowBlock = world.getBlock(x, y - 1, z);
			return (belowBlock != null) && (belowBlock.getMaterial() == Material.leaves );
		}
		return false;
	}

	@Override
	public boolean canBlockStay(World world, int x, int y, int z)
	{
		Block belowBlock = world.getBlock(x, y - 1, z);
		return (belowBlock != null) && (belowBlock.getMaterial() == Material.leaves);
	}

	@Override
	public void onNeighborBlockChange(World world, int i, int j, int k, Block l)
	{
		super.onNeighborBlockChange(world, i, j, k, l);
		if (!canBlockStay(world, i, j, k))
		{
			dropBlockAsItem(world, i, j, k, world.getBlockMetadata(i, j, k), 0);
			world.setBlockToAir(i, j, k);
		}
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int i, int j, int k)
	{
		return null;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@Override
	public int getRenderType()
	{
		return 1;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void randomDisplayTick(World world, int x, int y, int z, Random random)
	{
		if (random.nextFloat() > 0.1F)
		{
			return;
		}
		double px = x + random.nextDouble();
		double py = y + 0.5D + (random.nextDouble() - 0.5D) * 0.8D;
		double pz = z + random.nextDouble();
		EntityFX fx = new EntityAuraFX(world, px, py, pz, 0.0D, 0.0D, 0.0D);
		fx.setParticleTextureIndex(65);
		fx.setRBGColorF(1.0F, 1.0F, 0.0F);
		fx.multipleParticleScaleBy(0.7F);

		FMLClientHandler.instance().getClient().effectRenderer.addEffect(fx);
	}
}
