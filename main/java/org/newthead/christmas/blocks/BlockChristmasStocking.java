package org.newthead.christmas.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

import org.newthead.christmas.ChristmasCraft;
import org.newthead.christmas.ModSetup;
import org.newthead.christmas.tileentities.TileEntityStocking;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockChristmasStocking extends BlockContainer
{
	@SideOnly(Side.CLIENT)
	private IIcon[] icons;

	public BlockChristmasStocking()
	{
		super(Material.circuits);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister par1IconRegister)
	{
		int types = 4;
		this.icons = new IIcon[types];
		for (int i = 0; i < types; i++)
		{
			this.icons[i] = par1IconRegister.registerIcon(getTextureName() + "_" + i);
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int i, int j)
	{
		return this.icons[(j >> 2)];
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public int getRenderType()
	{
		return ModSetup.stockingRenderId;
	}

	@Override
	public int quantityDropped(Random random)
	{
		return 1;
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return new TileEntityStocking();
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int i, int j, int k)
	{
		return null;
	}

	@Override
	public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int x, int y, int z)
	{
		int meta = world.getBlockMetadata(x, y, z) & 0x3;
		float f = 0.125F;
		if (meta == 0) {
			setBlockBounds(0.0F, 0.0F, 1.0F - f, 1.0F, 1.0F, 1.0F);
		}
		if (meta == 1) {
			setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, f);
		}
		if (meta == 2) {
			setBlockBounds(1.0F - f, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
		}
		if (meta == 3) {
			setBlockBounds(0.0F, 0.0F, 0.0F, f, 1.0F, 1.0F);
		}
		return super.getSelectedBoundingBoxFromPool(world, x, y, z);
	}

	@Override
	public boolean canPlaceBlockAt(World world, int x, int y, int z)
	{
		if (world.getBlock(x - 1, y, z).isNormalCube()) {
			return true;
		}
		if (world.getBlock(x + 1, y, z).isNormalCube()) {
			return true;
		}
		if (world.getBlock(x, y, z - 1).isNormalCube()) {
			return true;
		}
		return world.getBlock(x, y, z + 1).isNormalCube();
	}

	@Override
	public int onBlockPlaced(World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ, int itemMeta)
	{
		int meta = world.getBlockMetadata(x, y, z);
		if (((meta == 0) || (side == 2)) && (world.getBlock(x, y, z + 1).isNormalCube())) {
			meta = 0;
		}
		if (((meta == 0) || (side == 3)) && (world.getBlock(x, y, z - 1).isNormalCube())) {
			meta = 1;
		}
		if (((meta == 0) || (side == 4)) && (world.getBlock(x + 1, y, z).isNormalCube())) {
			meta = 2;
		}
		if (((meta == 0) || (side == 5)) && (world.getBlock(x - 1, y, z).isNormalCube())) {
			meta = 3;
		}
		return meta;
	}

	@Override
	public void onNeighborBlockChange(World world, int i, int j, int k, Block l)
	{
		int meta = world.getBlockMetadata(i, j, k) & 0x3;
		boolean canStay = false;
		if ((meta == 0) && (world.getBlock(i, j, k + 1).isNormalCube())) {
			canStay = true;
		}
		if ((meta == 1) && (world.getBlock(i, j, k - 1).isNormalCube())) {
			canStay = true;
		}
		if ((meta == 2) && (world.getBlock(i + 1, j, k).isNormalCube())) {
			canStay = true;
		}
		if ((meta == 3) && (world.getBlock(i - 1, j, k).isNormalCube())) {
			canStay = true;
		}
		if (!canStay)
		{
			dropBlockAsItem(world, i, j, k, meta, 0);
			world.setBlockToAir(i, j, k);
		}
		super.onNeighborBlockChange(world, i, j, k, l);
	}

	@Override
	public void breakBlock(World world, int i, int j, int k, Block l, int m)
	{
		dropStockingContents(world, i, j, k);
		super.breakBlock(world, i, j, k, l, m);
	}

	@Override
	public boolean onBlockActivated(World world, int i, int j, int k, EntityPlayer entityplayer, int side, float f1, float f2, float f3)
	{
		ItemStack contents = null;
		TileEntityStocking tile = (TileEntityStocking)world.getTileEntity(i, j, k);
		if (tile != null && tile.getContents() != null)
		{
			contents = tile.getContents();
		}
		if (entityplayer.getCurrentEquippedItem() != null && contents == null)
		{
			ItemStack heldItem = entityplayer.getCurrentEquippedItem();
			if (Block.getBlockFromItem(heldItem.getItem()) == ChristmasCraft.blockPresent)
			{
				ItemStack newContents = heldItem.copy();
				newContents.stackSize = 1;
				tile.setContents(newContents);
				heldItem.stackSize -= 1;
				return true;
			}
		}
		else
		{
			dropStockingContents(world, i, j, k);
			return true;
		}
		return false;
	}

	private void dropStockingContents(World world, int x, int y, int z)
	{
		if (!world.isRemote)
		{
			TileEntityStocking tile = (TileEntityStocking)world.getTileEntity(x, y, z);
			if (tile != null && tile.getContents() != null)
			{
				ItemStack itemstack = tile.getContents();
				itemstack.stackSize = 1;
				dropBlockAsItem(world, x, y, z, itemstack);
				tile.setContents(null);
			}
		}
	}
}
