package org.newthead.christmas.blocks;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.particle.EntityAuraFX;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.IIcon;
import net.minecraft.world.ChunkPosition;
import net.minecraft.world.Explosion;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

import org.newthead.christmas.ChristmasCraft;
import org.newthead.christmas.ModSetup;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockChristmasLight extends Block
{
	@SideOnly(Side.CLIENT)
	private IIcon[] icons;
	private boolean isActivated;
	public static final int[] dyeColors = { 1, 2, 4, 11, 14 };
	public static final String[] names = { "red", "green", "blue", "yellow", "orange", "icicle" };

	public BlockChristmasLight(boolean activated)
	{
		super(Material.circuits);
		disableStats();
		this.isActivated = activated;
		if (activated)
		{
			setLightLevel(0.5F);
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister par1IconRegister)
	{
		this.icons = new IIcon[names.length];
		for (int i = 0; i < names.length; i++) {
			this.icons[i] = par1IconRegister.registerIcon(getTextureName() + "_" + names[i]);
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int meta)
	{
		return this.icons[(meta % names.length)];
	}

	@Override
	public Item getItemDropped(int i, Random random, int j)
	{
		return Item.getItemFromBlock(ChristmasCraft.blockChristmasLightOff);
	}

	@Override
	public int damageDropped(int i)
	{
		return i;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void getSubBlocks(Item block, CreativeTabs tabs, List inventoryItems)
	{
		if (!this.isActivated)
		{
			for (int meta = 0; meta < this.names.length; meta++)
			{
				inventoryItems.add(new ItemStack(block, 1, meta));
			}
		}
	}

	@Override
	public boolean canPlaceBlockAt(World world, int x, int y, int z)
	{
		return getValidSides(world, x, y, z) > 0;
	}

	private boolean isChristmasLightBlock(World world, int x, int y, int z)
	{
		return world.getBlock(x, y, z) instanceof BlockChristmasLight;
	}

	private boolean canLightGoAroundCorner(World world, int x, int y, int z)
	{
		if (world.isAirBlock(x, y, z))
		{
			return true;
		}
		if (world.getBlock(x, y, z).getMaterial().isReplaceable())
		{
			return true;
		}
		if (world.getBlock(x, y, z).getMaterial() == Material.plants)
		{
			return true;
		}
		return false;
	}

	@Override
	public void setBlockBoundsForItemRender()
	{
		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public int getRenderType()
	{
		return ModSetup.christmasLightRenderId;
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int par2, int par3, int par4)
	{
		return null;
	}

	@Override
	public void setBlockBoundsBasedOnState(IBlockAccess world, int x, int y, int z)
	{
		int sides = getValidSides(world, x, y, z);
		float x1 = 1.0F;
		float y1 = 1.0F;
		float z1 = 1.0F;
		float x2 = 0.0F;
		float y2 = 0.0F;
		float z2 = 0.0F;
		if ((sides & 0x2) != 0)
		{
			x2 = Math.max(x2, 0.0625F);
			x1 = 0.0F;
			y1 = 0.0F;
			y2 = 1.0F;
			z1 = 0.0F;
			z2 = 1.0F;
		}
		if ((sides & 0x8) != 0)
		{
			x1 = Math.min(x1, 0.9375F);
			x2 = 1.0F;
			y1 = 0.0F;
			y2 = 1.0F;
			z1 = 0.0F;
			z2 = 1.0F;
		}
		if ((sides & 0x4) != 0)
		{
			z2 = Math.max(z2, 0.0625F);
			z1 = 0.0F;
			x1 = 0.0F;
			x2 = 1.0F;
			y1 = 0.0F;
			y2 = 1.0F;
		}
		if ((sides & 0x1) != 0)
		{
			z1 = Math.min(z1, 0.9375F);
			z2 = 1.0F;
			x1 = 0.0F;
			x2 = 1.0F;
			y1 = 0.0F;
			y2 = 1.0F;
		}
		setBlockBounds(x1, y1, z1, x2, y2, z2);
	}

	private byte getValidSides(IBlockAccess world, int x, int y, int z)
	{
		byte byte0 = 0;
		if (canBePlacedOn(world, x, y, z + 1, ForgeDirection.NORTH)) {
			byte0 = (byte)(byte0 | 0x1);
		}
		if (canBePlacedOn(world, x - 1, y, z, ForgeDirection.EAST)) {
			byte0 = (byte)(byte0 | 0x2);
		}
		if (canBePlacedOn(world, x, y, z - 1, ForgeDirection.SOUTH)) {
			byte0 = (byte)(byte0 | 0x4);
		}
		if (canBePlacedOn(world, x + 1, y, z, ForgeDirection.WEST)) {
			byte0 = (byte)(byte0 | 0x8);
		}
		return byte0;
	}

	private boolean canBePlacedOn(IBlockAccess world, int x, int y, int z, ForgeDirection side)
	{
		return (world.getBlock(x, y, z).getMaterial().isSolid()) && (world.isSideSolid(x, y, z, side, false));
	}

	@Override
	public void onBlockAdded(World world, int i, int j, int k)
	{
		super.onBlockAdded(world, i, j, k);
		updateAndPropagateLightPower(world, i, j, k, true);
	}

	@Override
	public void onBlockDestroyedByExplosion(World world, int x, int y, int z, Explosion explosion)
	{
		super.onBlockDestroyedByExplosion(world, x, y, z, explosion);
		onBlockRemoval(world, x, y, z);
	}

	@Override
	public void onBlockDestroyedByPlayer(World world, int x, int y, int z, int meta)
	{
		super.onBlockDestroyedByPlayer(world, x, y, z, meta);
		onBlockRemoval(world, x, y, z);
	}

	private void onBlockRemoval(World world, int i, int j, int k)
	{
		updateAndPropagateLightPower(world, i, j, k, true);
	}

	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, Block block)
	{
		if (world.isRemote)
		{
			return;
		}
		super.onNeighborBlockChange(world, x, y, z, block);
		int meta = world.getBlockMetadata(x, y, z);
		boolean flag = canPlaceBlockAt(world, x, y, z);
		if (!flag)
		{
			dropBlockAsItem(world, x, y, z, meta, 0);
			world.setBlockToAir(x, y, z);
		}
		else
		{
			updateAndPropagateLightPower(world, x, y, z, true);
		}
	}

	private void updateAndPropagateLightPower(World world, int x, int y, int z, boolean forceUpdate)
	{
		if (world.isRemote) {
			return;
		}
		boolean isPowered;
		if (isChristmasLightBlock(world, x, y, z))
		{
			Set<ChunkPosition> lightsNeedingUpdate = new HashSet();
			calculateLightChanges(world, x, y, z, lightsNeedingUpdate);
			isPowered = false;
			for (ChunkPosition pos : lightsNeedingUpdate)
			{
				int l = getValidSides(world, pos.chunkPosX, pos.chunkPosY, pos.chunkPosZ);
				if (world.isBlockIndirectlyGettingPowered(pos.chunkPosX, pos.chunkPosY, pos.chunkPosZ))
				{
					isPowered = true;
					break;
				}
			}
			for (ChunkPosition pos : lightsNeedingUpdate)
			{
				int l = world.getBlockMetadata(pos.chunkPosX, pos.chunkPosY, pos.chunkPosZ);
				if (isPowered)
				{
					world.setBlock(pos.chunkPosX, pos.chunkPosY, pos.chunkPosZ, ChristmasCraft.blockChristmasLightOn, l, 3);
				}
				else
				{
					world.setBlock(pos.chunkPosX, pos.chunkPosY, pos.chunkPosZ, ChristmasCraft.blockChristmasLightOff, l, 3);
				}
			}
		}
		else if (forceUpdate)
		{
			for (int xOff = -1; xOff < 2; xOff++) {
				for (int zOff = -1; zOff < 2; zOff++) {
					if ((xOff != 0) || (zOff != 0))
					{
						int newX = x + xOff;
						int newZ = z + zOff;
						updateAndPropagateLightPower(world, newX, y, newZ, false);
					}
				}
			}
		}
	}

	private void calculateLightChanges(World world, int x, int y, int z, Set lightsNeedingUpdate)
	{
		ChunkPosition p = new ChunkPosition(x, y, z);
		if (lightsNeedingUpdate.contains(p)) {
			return;
		}
		if (isChristmasLightBlock(world, x, y, z))
		{
			lightsNeedingUpdate.add(p);
			int m = getValidSides(world, x, y, z);
			if ((m & 0x1) > 0)
			{
				if ((isChristmasLightBlock(world, x - 1, y, z + 1)) && (canLightGoAroundCorner(world, x - 1, y, z))) {
					calculateLightChanges(world, x - 1, y, z + 1, lightsNeedingUpdate);
				}
				if ((isChristmasLightBlock(world, x - 1, y, z)) && ((getValidSides(world, x - 1, y, z) & 0x1) > 0)) {
					calculateLightChanges(world, x - 1, y, z, lightsNeedingUpdate);
				}
				if ((isChristmasLightBlock(world, x + 1, y, z)) && ((getValidSides(world, x + 1, y, z) & 0x1) > 0)) {
					calculateLightChanges(world, x + 1, y, z, lightsNeedingUpdate);
				}
				if ((isChristmasLightBlock(world, x + 1, y, z + 1)) && (canLightGoAroundCorner(world, x + 1, y, z))) {
					calculateLightChanges(world, x + 1, y, z + 1, lightsNeedingUpdate);
				}
			}
			if ((m & 0x2) > 0)
			{
				if ((isChristmasLightBlock(world, x - 1, y, z - 1)) && (canLightGoAroundCorner(world, x, y, z - 1))) {
					calculateLightChanges(world, x - 1, y, z - 1, lightsNeedingUpdate);
				}
				if ((isChristmasLightBlock(world, x, y, z - 1)) && ((getValidSides(world, x, y, z - 1) & 0x2) > 0)) {
					calculateLightChanges(world, x, y, z - 1, lightsNeedingUpdate);
				}
				if ((isChristmasLightBlock(world, x, y, z + 1)) && ((getValidSides(world, x, y, z + 1) & 0x2) > 0)) {
					calculateLightChanges(world, x, y, z + 1, lightsNeedingUpdate);
				}
				if ((isChristmasLightBlock(world, x - 1, y, z + 1)) && (canLightGoAroundCorner(world, x, y, z + 1))) {
					calculateLightChanges(world, x - 1, y, z + 1, lightsNeedingUpdate);
				}
			}
			if ((m & 0x4) > 0)
			{
				if ((isChristmasLightBlock(world, x - 1, y, z - 1)) && (canLightGoAroundCorner(world, x - 1, y, z))) {
					calculateLightChanges(world, x - 1, y, z - 1, lightsNeedingUpdate);
				}
				if ((isChristmasLightBlock(world, x - 1, y, z)) && ((getValidSides(world, x - 1, y, z) & 0x4) > 0)) {
					calculateLightChanges(world, x - 1, y, z, lightsNeedingUpdate);
				}
				if ((isChristmasLightBlock(world, x + 1, y, z)) && ((getValidSides(world, x + 1, y, z) & 0x4) > 0)) {
					calculateLightChanges(world, x + 1, y, z, lightsNeedingUpdate);
				}
				if ((isChristmasLightBlock(world, x + 1, y, z - 1)) && (canLightGoAroundCorner(world, x + 1, y, z))) {
					calculateLightChanges(world, x + 1, y, z - 1, lightsNeedingUpdate);
				}
			}
			if ((m & 0x8) > 0)
			{
				if ((isChristmasLightBlock(world, x + 1, y, z - 1)) && (canLightGoAroundCorner(world, x, y, z - 1))) {
					calculateLightChanges(world, x + 1, y, z - 1, lightsNeedingUpdate);
				}
				if ((isChristmasLightBlock(world, x, y, z - 1)) && ((getValidSides(world, x, y, z - 1) & 0x8) > 0)) {
					calculateLightChanges(world, x, y, z - 1, lightsNeedingUpdate);
				}
				if ((isChristmasLightBlock(world, x, y, z + 1)) && ((getValidSides(world, x, y, z + 1) & 0x8) > 0)) {
					calculateLightChanges(world, x, y, z + 1, lightsNeedingUpdate);
				}
				if ((isChristmasLightBlock(world, x + 1, y, z + 1)) && (canLightGoAroundCorner(world, x, y, z + 1))) {
					calculateLightChanges(world, x + 1, y, z + 1, lightsNeedingUpdate);
				}
			}
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void randomDisplayTick(World world, int x, int y, int z, Random random)
	{
		if (this.isActivated && world.getBlockMetadata(x, y, z) == 5)
		{
			if (random.nextFloat() > 0.2F)
			{
				return;
			}
			byte sides = getValidSides(world, x, y, z);
			if (sides == 0)
			{
				return;
			}
			List<double[]> offsets = new ArrayList();
			if ((sides & 0x1) > 0) {
				offsets.add(new double[] { 0.0D, 0.9D });
			}
			if ((sides & 0x2) > 0) {
				offsets.add(new double[] { 0.1D, 0.0D });
			}
			if ((sides & 0x4) > 0) {
				offsets.add(new double[] { 0.0D, 0.1D });
			}
			if ((sides & 0x8) > 0) {
				offsets.add(new double[] { 0.9D, 0.0D });
			}
			double randU = random.nextDouble();
			double randV = random.nextDouble() * 0.7D;
			double[] side = (double[])offsets.get(random.nextInt(offsets.size()));
			double dx = x + (side[0] == 0.0D ? randU : side[0]);
			double dy = y + randV;
			double dz = z + (side[1] == 0.0D ? randU : side[1]);

			EntityFX fx = new EntityAuraFX(world, dx, dy, dz, 0.0D, 0.0D, 0.0D);
			fx.motionX = 0.0D;
			fx.motionY = 0.0D;
			fx.motionZ = 0.0D;
			fx.noClip = false;
			fx.setParticleTextureIndex(65);
			fx.setRBGColorF(1.0F, 1.0F, 0.8F);
			fx.setAlphaF(0.7F);
			fx.multipleParticleScaleBy(0.5F);
			FMLClientHandler.instance().getClient().effectRenderer.addEffect(fx);
		}
	}
}
