package org.newthead.christmas.blocks;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLeavesBase;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.IIcon;
import net.minecraft.util.Vec3;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import org.newthead.christmas.ModSetup;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockOrnament extends Block
{
	public static final int[] dyeColors = { 1, 2, 4, 11, 14 };
	public static final String[] names = { "red", "green", "blue", "yellow", "orange" };
	@SideOnly(Side.CLIENT)
	private IIcon[] icons;

	public BlockOrnament()
	{
		super(Material.glass);
		disableStats();
		setLightLevel(0.25F);
	}

	@Override
	public int damageDropped(int i)
	{
		return i;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void getSubBlocks(Item block, CreativeTabs tabs, List inventoryItems)
	{
		for (int meta = 0; meta < names.length; meta++)
		{
			inventoryItems.add(new ItemStack(block, 1, meta));
		}
	}

	@Override
	public void registerBlockIcons(IIconRegister par1IconRegister)
	{
		this.icons = new IIcon[names.length];
		for (int i = 0; i < names.length; i++)
		{
			this.icons[i] = par1IconRegister.registerIcon(getTextureName() + "_" + names[i]);
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int meta)
	{
		return this.icons[meta];
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public int getRenderType()
	{
		return ModSetup.ornamentRenderId;
	}

	private byte getValidSides(IBlockAccess world, int i, int j, int k)
	{
		byte sides = 0;
		if (canAttachTo(world.getBlock(i, j, k + 1))) {
			sides = (byte)(sides | 0x1);
		}
		if (canAttachTo(world.getBlock(i, j, k - 1))) {
			sides = (byte)(sides | 0x4);
		}
		if (canAttachTo(world.getBlock(i + 1, j, k))) {
			sides = (byte)(sides | 0x8);
		}
		if (canAttachTo(world.getBlock(i - 1, j, k))) {
			sides = (byte)(sides | 0x2);
		}
		if (canAttachTo(world.getBlock(i, j + 1, k))) {
			sides = (byte)(sides | 0x10);
		}
		return sides;
	}

	private boolean canAttachTo(Block block)
	{
		return block instanceof BlockLeavesBase;
	}

	@Override
	public boolean canPlaceBlockAt(World world, int i, int j, int k)
	{
		return getValidSides(world, i, j, k) > 0;
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int i, int j, int k)
	{
		return null;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int i, int j, int k)
	{
		onBlockAdded(world, i, j, k);
		return super.getSelectedBoundingBoxFromPool(world, i, j, k);
	}

	@Override
	public void setBlockBoundsBasedOnState(IBlockAccess world, int x, int y, int z)
	{
		float r = 0.2F;
		Vec3 offset = getBlockOffsets(world, x, y, z);
		offset = offset.addVector(0.5D, 0.5D, 0.5D);
		setBlockBounds((float)offset.xCoord - r, (float)offset.yCoord - r, (float)offset.zCoord - r, (float)offset.xCoord + r, (float)offset.yCoord + r, (float)offset.zCoord + r);
	}

	public Vec3 getBlockOffsets(IBlockAccess world, int x, int y, int z)
	{
		byte sides = getValidSides(world, x, y, z);
		float offsetX = 0.0F;
		float offsetY = 0.0F;
		float offsetZ = 0.0F;
		float d = 0.4F;
		if ((sides & 0x10) > 0)
		{
			long l1 = x * 3129871 ^ z * 116129781L ^ y;
			l1 = l1 * l1 * 42317861L + l1 * 11L;
			offsetX = (float)(offsetX + ((float)(l1 >> 16 & 0xF) / 15.0F - 0.5D) * 0.4D);
			offsetZ = (float)(offsetZ + ((float)(l1 >> 24 & 0xF) / 15.0F - 0.5D) * 0.4D);
			offsetY += 0.25F;
		}
		else
		{
			if ((sides & 0x1) > 0) {
				offsetZ += d;
			}
			if ((sides & 0x2) > 0) {
				offsetX -= d;
			}
			if ((sides & 0x4) > 0) {
				offsetZ -= d;
			}
			if ((sides & 0x8) > 0) {
				offsetX += d;
			}
			long l1 = x * 3129871 ^ z * 116129781L ^ y;
			l1 = l1 * l1 * 42317861L + l1 * 11L;
			if ((sides & 0xF) > 0 && (world.getBlock(x, y - 1, z) == Blocks.air || world.getBlock(x, y - 1, z) == this))
			{
				offsetY = (float)(offsetY + (((float)(l1 >> 20 & 0xF) / 15.0F - 0.5D) * 0.3D - 0.3D));
			}
			else
			{
				offsetY = (float)(offsetY + ((float)(l1 >> 20 & 0xF) / 15.0F - 0.5D) * 0.4D);
			}
		}
		return Vec3.createVectorHelper(offsetX, offsetY, offsetZ);
	}

	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, Block block)
	{
		if (!canPlaceBlockAt(world, x, y, z))
		{
			int meta = world.getBlockMetadata(x, y, z);
			dropBlockAsItem(world, x, y, z, meta, 0);
			world.setBlockToAir(x, y, z);
		}
		super.onNeighborBlockChange(world, x, y, z, block);
	}
}
