package org.newthead.christmas.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;

import org.newthead.christmas.ModSetup;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockWreath extends Block
{
	public BlockWreath()
	{
		super(Material.leaves);
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public int getRenderType()
	{
		return ModSetup.wreathRenderId;
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int x, int y, int z)
	{
		return null;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int x, int y, int z)
	{
		int meta = world.getBlockMetadata(x, y, z);
		float offset = 0.25F;
		if (meta == 2) {
			setBlockBounds(0.0F, 0.0F, 1.0F - offset, 1.0F, 1.0F, 1.0F);
		}
		if (meta == 3) {
			setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, offset);
		}
		if (meta == 4) {
			setBlockBounds(1.0F - offset, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
		}
		if (meta == 5) {
			setBlockBounds(0.0F, 0.0F, 0.0F, offset, 1.0F, 1.0F);
		}
		return super.getSelectedBoundingBoxFromPool(world, x, y, z);
	}

	@Override
	public boolean canPlaceBlockAt(World world, int x, int y, int z)
	{
		if (world.getBlock(x - 1, y, z).isNormalCube()) {
			return true;
		}
		if (world.getBlock(x + 1, y, z).isNormalCube()) {
			return true;
		}
		if (world.getBlock(x, y, z - 1).isNormalCube()) {
			return true;
		}
		return world.getBlock(x, y, z + 1).isNormalCube();
	}

	@Override
	public int onBlockPlaced(World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ, int itemMeta)
	{
		int meta = world.getBlockMetadata(x, y, z);
		if (((meta == 0) || (side == 2)) && (world.getBlock(x, y, z + 1).isNormalCube())) {
			meta = 2;
		}
		if (((meta == 0) || (side == 3)) && (world.getBlock(x, y, z - 1).isNormalCube())) {
			meta = 3;
		}
		if (((meta == 0) || (side == 4)) && (world.getBlock(x + 1, y, z).isNormalCube())) {
			meta = 4;
		}
		if (((meta == 0) || (side == 5)) && (world.getBlock(x - 1, y, z).isNormalCube())) {
			meta = 5;
		}
		return meta;
	}

	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, Block changed)
	{
		int meta = world.getBlockMetadata(x, y, z);
		boolean canStay = false;
		if ((meta == 2) && (world.getBlock(x, y, z + 1).isNormalCube())) {
			canStay = true;
		}
		if ((meta == 3) && (world.getBlock(x, y, z - 1).isNormalCube())) {
			canStay = true;
		}
		if ((meta == 4) && (world.getBlock(x + 1, y, z).isNormalCube())) {
			canStay = true;
		}
		if ((meta == 5) && (world.getBlock(x - 1, y, z).isNormalCube())) {
			canStay = true;
		}
		if (!canStay)
		{
			dropBlockAsItem(world, x, y, z, 0, 0);
			world.setBlockToAir(x, y, z);
		}
		super.onNeighborBlockChange(world, x, y, z, changed);
	}
}
