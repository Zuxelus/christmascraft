package org.newthead.christmas.blocks;

import net.minecraft.block.BlockCake;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

import org.newthead.christmas.ChristmasCraft;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockFruitcake extends BlockCake
{
	@SideOnly(Side.CLIENT)
	private IIcon cakeInnerIcon;
	@SideOnly(Side.CLIENT)
	private IIcon a;
	@SideOnly(Side.CLIENT)
	private IIcon b;

	public BlockFruitcake()
	{
		super();
		disableStats();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister par1IconRegister)
	{
		this.blockIcon = par1IconRegister.registerIcon(getTextureName() + "_side");
		this.cakeInnerIcon = par1IconRegister.registerIcon(getTextureName() + "_inner");
		this.a = par1IconRegister.registerIcon(getTextureName() + "_top");
		this.b = par1IconRegister.registerIcon(getTextureName() + "_bottom");
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int par1, int par2)
	{
		return (par2 > 0) && (par1 == 4) ? this.cakeInnerIcon : par1 == 0 ? this.b : par1 == 1 ? this.a : this.blockIcon;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public Item getItem(World world, int par2, int par3, int par4)
	{
		return ChristmasCraft.itemFruitcake;
	}
}
