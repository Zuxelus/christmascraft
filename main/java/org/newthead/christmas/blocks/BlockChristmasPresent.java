package org.newthead.christmas.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import org.newthead.christmas.ModSetup;
import org.newthead.christmas.tileentities.TileEntityChristmasPresent;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockChristmasPresent extends BlockContainer
{
	@SideOnly(Side.CLIENT)
	private IIcon[] bottomIcons;
	@SideOnly(Side.CLIENT)
	private IIcon[] topIcons;
	@SideOnly(Side.CLIENT)
	private IIcon[] sideIcons;
	@SideOnly(Side.CLIENT)
	public IIcon bowIcon;
	public static final float[] blockBounds = { 0.0625F, 0.0F, 0.0625F, 0.9375F, 0.8125F, 0.9375F, 0.0F, 0.0F, 0.0F, 1.0F, 0.5625F, 1.0F, 0.125F, 0.0F, 0.125F, 0.875F, 0.75F, 0.875F, 0.0F, 0.0F, 0.0F, 1.0F, 0.8125F, 1.0F };
	public static final float[] blockRenderBounds = { 0.125F, 0.0F, 0.125F, 0.875F, 0.625F, 0.875F, 0.0625F, 0.625F, 0.0625F, 0.9375F, 0.8125F, 0.9375F, 0.0F, 0.0F, 0.0F, 1.0F, 0.5625F, 1.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.5625F, 1.0F, 0.1875F, 0.0F, 0.1875F, 0.8125F, 0.5F, 0.8125F, 0.125F, 0.5F, 0.125F, 0.875F, 0.75F, 0.875F, 0.0625F, 0.0F, 0.0625F, 0.9375F, 0.6875F, 0.9375F, 0.0F, 0.6875F, 0.0F, 1.0F, 0.8125F, 1.0F };
	public static final float[] bowHeight = { 0.1875F, 0.25F, 0.1875F, 0.1875F };

	public BlockChristmasPresent()
	{
		super(Material.cloth);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public String getItemIconName()
	{
		return getTextureName();
	}

	@Override
	public void registerBlockIcons(IIconRegister par1IconRegister)
	{
		int types = 4;
		this.bottomIcons = new IIcon[types];
		this.topIcons = new IIcon[types];
		this.sideIcons = new IIcon[types];
		for (int i = 0; i < types; i++)
		{
			this.bottomIcons[i] = par1IconRegister.registerIcon(getTextureName() + "_" + i + "_bottom");
			this.topIcons[i] = par1IconRegister.registerIcon(getTextureName() + "_" + i + "_top");
			this.sideIcons[i] = par1IconRegister.registerIcon(getTextureName() + "_" + i + "_side");
		}
		this.bowIcon = par1IconRegister.registerIcon(getTextureName() + "_bows");
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return new TileEntityChristmasPresent();
	}

	@Override
	public Item getItemDropped(int par1, Random par2Random, int par3)
	{
		return null;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public boolean canBlockStay(World world, int i, int j, int k)
	{
		return world.getBlock(i, j - 1, k).getMaterial().isSolid();
	}

	@Override
	public boolean canPlaceBlockAt(World world, int i, int j, int k)
	{
		if (!super.canPlaceBlockAt(world, i, j, k))
		{
			return false;
		}
		return (canBlockStay(world, i, j, k)) && (world.getBlock(i, j - 1, k) != this);
	}

	@Override
	public void onNeighborBlockChange(World world, int i, int j, int k, Block l)
	{
		if (!canBlockStay(world, i, j, k))
		{
			dropBlockAsItem(world, i, j, k, world.getBlockMetadata(i, j, k), 0);
			world.setBlockToAir(i, j, k);
		}
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@Override
	public int getRenderType()
	{
		return ModSetup.presentRenderId;
	}

	@Override
	public IIcon getIcon(int side, int meta)
	{
		int type = meta & 0x3;
		if (side == 0)
		{
			return this.bottomIcons[type];
		}
		if (side == 1)
		{
			return this.topIcons[type];
		}
		return this.sideIcons[type];
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int x, int y, int z)
	{
		int giftType = world.getBlockMetadata(x, y, z);
		int offset = giftType * 6;
		if (offset >= blockBounds.length)
		{
			offset = 0;
		}
		return AxisAlignedBB.getBoundingBox(x + blockBounds[offset], y + blockBounds[(offset + 1)], z + blockBounds[(offset + 2)], x + blockBounds[(offset + 3)], y + blockBounds[(offset + 4)], z + blockBounds[(offset + 5)]);
	}

	@Override
	public AxisAlignedBB getSelectedBoundingBoxFromPool(World world, int i, int j, int k)
	{
		int giftType = world.getBlockMetadata(i, j, k);
		int offset = giftType * 6;
		if (offset >= blockBounds.length)
		{
			offset = 0;
		}
		return AxisAlignedBB.getBoundingBox(i + blockBounds[offset], j, k + blockBounds[(offset + 2)], i + blockBounds[(offset + 3)], j + blockBounds[(offset + 4)] + bowHeight[giftType], k + blockBounds[(offset + 5)]);
	}

	public void setXmasBoxBounds(IBlockAccess world, int x, int y, int z, int renderType)
	{
		int metadata = world.getBlockMetadata(x, y, z);
		setXmasBoxBounds(metadata, renderType);
	}

	public void setXmasBoxBounds(int metadata, int renderType)
	{
		if (renderType == 0)
		{
			int offset = metadata * 6;
			setBlockBounds(blockBounds[offset], blockBounds[(offset + 1)], blockBounds[(offset + 2)], blockBounds[(offset + 3)], blockBounds[(offset + 4)], blockBounds[(offset + 5)]);
		}
		else
		{
			int offset = metadata * 12 + (renderType - 1) * 6;
			setBlockBounds(blockRenderBounds[offset], blockRenderBounds[(offset + 1)], blockRenderBounds[(offset + 2)], blockRenderBounds[(offset + 3)], blockRenderBounds[(offset + 4)], blockRenderBounds[(offset + 5)]);
		}
	}

	public float getBowHeight(IBlockAccess world, int x, int y, int z)
	{
		return getBowHeight(world.getBlockMetadata(x, y, z));
	}

	public float getBowHeight(int metadata)
	{
		int i = metadata * 12;
		if (i >= blockRenderBounds.length) {
			i = 0;
		}
		return blockRenderBounds[(i + 10)];
	}

	@Override
	public void breakBlock(World world, int x, int y, int z, Block block, int meta)
	{
		TileEntityChristmasPresent tile = (TileEntityChristmasPresent)world.getTileEntity(x, y, z);
		if (tile != null && tile.getContents() != null)
		{
			ItemStack itemstack = tile.getContents();
			itemstack.stackSize = 1;
			dropBlockAsItem(world, x, y, z, itemstack);
			tile.setContents(null);
		}
		super.breakBlock(world, x, y, z, block, meta);
	}

	@Override
	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack itemStack)
	{
		TileEntityChristmasPresent tile = (TileEntityChristmasPresent)world.getTileEntity(x, y, z);
		if (tile != null)
		{
			ItemStack tempStack = itemStack.copy();
			tempStack.stackSize = 1;
			tile.setContents(tempStack);
		}
	}
}
