package org.newthead.christmas.models;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.model.TextureOffset;
import net.minecraft.entity.Entity;
import net.minecraft.util.MathHelper;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModelIceSkates extends ModelBase
{
	public ModelRenderer bipedHeadwear;
	public ModelRenderer bipedBody;
	public ModelRenderer bipedRightLeg;
	public ModelRenderer bipedLeftLeg;
	public boolean isSneak;

	public ModelIceSkates()
	{
		this(0.0F);
	}

	public ModelIceSkates(float par1)
	{
		this(par1, 0.0F, 16, 32);
	}

	public ModelIceSkates(float par1, float par2, int par3, int par4)
	{
		this.textureWidth = par3;
		this.textureHeight = par4;

		setTextureOffset("Headwear.head", 32, 0);
		setTextureOffset("RightLeg.skate", 0, 16);
		setTextureOffset("RightLeg.blade", 0, 0);
		setTextureOffset("LeftLeg.skate", 0, 16);
		setTextureOffset("LeftLeg.blade", 0, 0);

		this.bipedHeadwear = new ModelRenderer(this, "Headwear");
		addBox(this.bipedHeadwear, "head", -4.0F, -8.0F, -4.0F, 8, 8, 8, par1 + 0.5F);
		this.bipedHeadwear.setRotationPoint(0.0F, 0.0F + par2, 0.0F);

		this.bipedBody = new ModelRenderer(this, 16, 16);
		this.bipedBody.addBox(-4.0F, 0.0F, -2.0F, 8, 12, 4, par1);
		this.bipedBody.setRotationPoint(0.0F, 0.0F + par2, 0.0F);

		this.bipedRightLeg = new ModelRenderer(this, "RightLeg");
		addBox(this.bipedRightLeg, "skate", -2.0F, 0.0F, -2.0F, 4, 12, 4, par1);
		this.bipedRightLeg.setRotationPoint(-1.9F, 12.0F + par2, 0.0F);
		addBox(this.bipedRightLeg, "blade", -0.5F, 12.5F, -2.0F, 1, 1, 4);

		this.bipedLeftLeg = new ModelRenderer(this, "LeftLeg");
		this.bipedLeftLeg.mirror = true;
		addBox(this.bipedLeftLeg, "skate", -2.0F, 0.0F, -2.0F, 4, 12, 4, par1);
		this.bipedLeftLeg.setRotationPoint(1.9F, 12.0F + par2, 0.0F);
		addBox(this.bipedLeftLeg, "blade", -0.5F, 12.5F, -2.0F, 1, 1, 4);
	}

	@Override
	public void render(Entity par1Entity, float par2, float par3, float par4, float par5, float par6, float par7)
	{
		setRotationAngles(par2, par3, par4, par5, par6, par7, par1Entity);
		if (this.isChild)
		{
			float f6 = 2.0F;
			GL11.glPushMatrix();
			GL11.glScalef(1.5F / f6, 1.5F / f6, 1.5F / f6);
			GL11.glTranslatef(0.0F, 16.0F * par7, 0.0F);
			GL11.glPopMatrix();
			GL11.glPushMatrix();
			GL11.glScalef(1.0F / f6, 1.0F / f6, 1.0F / f6);
			GL11.glTranslatef(0.0F, 24.0F * par7, 0.0F);
			this.bipedBody.render(par7);
			this.bipedRightLeg.render(par7);
			this.bipedLeftLeg.render(par7);
			this.bipedHeadwear.render(par7);
			GL11.glPopMatrix();
		}
		else
		{
			this.bipedBody.render(par7);
			this.bipedRightLeg.render(par7);
			this.bipedLeftLeg.render(par7);
			this.bipedHeadwear.render(par7);
		}
	}

	@Override
	public void setRotationAngles(float par1, float par2, float par3, float par4, float par5, float par6, Entity par7Entity)
	{
		this.bipedHeadwear.rotateAngleY = (par4 / 57.295776F);
		this.bipedHeadwear.rotateAngleX = (par5 / 57.295776F);

		this.bipedRightLeg.rotateAngleX = (MathHelper.cos(par1 * 0.6662F) * 1.4F * par2);
		this.bipedLeftLeg.rotateAngleX = (MathHelper.cos(par1 * 0.6662F + 3.141593F) * 1.4F * par2);
		this.bipedRightLeg.rotateAngleY = 0.0F;
		this.bipedLeftLeg.rotateAngleY = 0.0F;
		if (this.isRiding)
		{
			this.bipedRightLeg.rotateAngleX = -1.256637F;
			this.bipedLeftLeg.rotateAngleX = -1.256637F;
			this.bipedRightLeg.rotateAngleY = 0.3141593F;
			this.bipedLeftLeg.rotateAngleY = -0.3141593F;
		}
		if (this.onGround > -9990.0F)
		{
			float f6 = this.onGround;
			this.bipedBody.rotateAngleY = (MathHelper.sin(MathHelper.sqrt_float(f6) * 3.141593F * 2.0F) * 0.2F);
		}
		if (this.isSneak)
		{
			this.bipedBody.rotateAngleX = 0.5F;

			this.bipedRightLeg.rotationPointZ = 4.0F;
			this.bipedLeftLeg.rotationPointZ = 4.0F;
			this.bipedRightLeg.rotationPointY = 9.0F;
			this.bipedLeftLeg.rotationPointY = 9.0F;
			this.bipedHeadwear.rotationPointY = 1.0F;
		}
		else
		{
			this.bipedBody.rotateAngleX = 0.0F;
			this.bipedRightLeg.rotationPointZ = 0.1F;
			this.bipedLeftLeg.rotationPointZ = 0.1F;
			this.bipedRightLeg.rotationPointY = 12.0F;
			this.bipedLeftLeg.rotationPointY = 12.0F;
			this.bipedHeadwear.rotationPointY = 0.0F;
		}
	}

	private void addBox(ModelRenderer m, String s, float x, float y, float z, int w, int h, int l)
	{
		addBox(m, s, x, y, z, w, h, l, 0.0F);
	}

	private void addBox(ModelRenderer m, String s, float x, float y, float z, int w, int h, int l, float f)
	{
		s = m.boxName + "." + s;
		TextureOffset textureoffset = getTextureOffset(s);
		int offX = textureoffset.textureOffsetX;
		int offY = textureoffset.textureOffsetY;
		m.cubeList.add(new ModelBox(m, offX, offY, x, y, z, w, h, l, f).func_78244_a(s));
	}
}
