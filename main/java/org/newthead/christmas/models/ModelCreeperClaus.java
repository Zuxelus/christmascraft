package org.newthead.christmas.models;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.model.TextureOffset;
import net.minecraft.entity.Entity;
import net.minecraft.util.MathHelper;

public class ModelCreeperClaus extends ModelBase
{
	public ModelRenderer head;
	public ModelRenderer unusedCreeperHeadwear;
	public ModelRenderer body;
	public ModelRenderer leg1;
	public ModelRenderer leg2;
	public ModelRenderer leg3;
	public ModelRenderer leg4;

	public ModelCreeperClaus()
	{
		this(0.0F);
	}

	public ModelCreeperClaus(float f)
	{
		int i = 4;

		setTextureOffset("Head.head", 0, 0);
		setTextureOffset("Head.hat1", 32, 6);
		setTextureOffset("Head.hat2", 40, 20);
		setTextureOffset("Head.hat3", 48, 0);
		setTextureOffset("Head.hat4", 36, 16);

		this.head = new ModelRenderer(this, "Head");
		addBox(this.head, "head", -4.0F, -8.0F, -4.0F, 8, 8, 8, f);
		addBox(this.head, "hat1", -4.0F, -10.0F, -4.0F, 8, 2, 8, f);
		addBox(this.head, "hat2", -3.0F, -13.0F, -3.0F, 6, 3, 6, f);
		addBox(this.head, "hat3", -2.0F, -15.0F, -2.0F, 4, 2, 4, f);
		addBox(this.head, "hat4", -1.0F, -17.0F, -1.0F, 2, 2, 2, f);
		this.head.setRotationPoint(0.0F, i, 0.0F);
		this.unusedCreeperHeadwear = new ModelRenderer(this, 32, 0);
		this.unusedCreeperHeadwear.addBox(-4.0F, -8.0F, -4.0F, 8, 8, 8, f + 0.5F);
		this.unusedCreeperHeadwear.setRotationPoint(0.0F, i, 0.0F);
		this.body = new ModelRenderer(this, 16, 16);
		this.body.addBox(-4.0F, 0.0F, -2.0F, 8, 12, 4, f);
		this.body.setRotationPoint(0.0F, i, 0.0F);
		this.leg1 = new ModelRenderer(this, 0, 16);
		this.leg1.addBox(-2.0F, 0.0F, -2.0F, 4, 6, 4, f);
		this.leg1.setRotationPoint(-2.0F, 12 + i, 4.0F);
		this.leg2 = new ModelRenderer(this, 0, 16);
		this.leg2.addBox(-2.0F, 0.0F, -2.0F, 4, 6, 4, f);
		this.leg2.setRotationPoint(2.0F, 12 + i, 4.0F);
		this.leg3 = new ModelRenderer(this, 0, 16);
		this.leg3.addBox(-2.0F, 0.0F, -2.0F, 4, 6, 4, f);
		this.leg3.setRotationPoint(-2.0F, 12 + i, -4.0F);
		this.leg4 = new ModelRenderer(this, 0, 16);
		this.leg4.addBox(-2.0F, 0.0F, -2.0F, 4, 6, 4, f);
		this.leg4.setRotationPoint(2.0F, 12 + i, -4.0F);
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		setRotationAngles(f, f1, f2, f3, f4, f5, entity);
		this.head.render(f5);
		this.body.render(f5);
		this.leg1.render(f5);
		this.leg2.render(f5);
		this.leg3.render(f5);
		this.leg4.render(f5);
	}

	@Override
	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
	{
		this.head.rotateAngleY = (f3 / 57.29578F);
		this.head.rotateAngleX = (f4 / 57.29578F);
		this.leg1.rotateAngleX = (MathHelper.cos(f * 0.6662F) * 1.4F * f1);
		this.leg2.rotateAngleX = (MathHelper.cos(f * 0.6662F + 3.141593F) * 1.4F * f1);
		this.leg3.rotateAngleX = (MathHelper.cos(f * 0.6662F + 3.141593F) * 1.4F * f1);
		this.leg4.rotateAngleX = (MathHelper.cos(f * 0.6662F) * 1.4F * f1);
	}

	private void addBox(ModelRenderer m, String s, float x, float y, float z, int w, int h, int l, float f)
	{
		s = m.boxName + "." + s;
		TextureOffset textureoffset = getTextureOffset(s);
		int offX = textureoffset.textureOffsetX;
		int offY = textureoffset.textureOffsetY;
		m.cubeList.add(new ModelBox(m, offX, offY, x, y, z, w, h, l, f).func_78244_a(s));
	}
}
