package org.newthead.christmas.renderers;

import java.util.Random;

import net.minecraft.block.material.Material;
import net.minecraft.client.model.ModelEnderman;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.newthead.christmas.entities.EntityEnderElf;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderEnderElf extends RenderLiving
{
	private static final ResourceLocation enderelfEyesTexture = new ResourceLocation("textures/entity/enderman/enderman_eyes.png");
	private static final ResourceLocation enderelfTextures = new ResourceLocation("christmascraft", "textures/models/enderelf.png");
	private ModelEnderman enderElfModel;
	private Random rnd = new Random();

	public RenderEnderElf()
	{
		super(new ModelEnderman(), 0.5F);
		this.enderElfModel = ((ModelEnderman)this.mainModel);
		setRenderPassModel(this.enderElfModel);
	}

	public void doRender(EntityEnderElf par1EntityEnderelf, double par2, double par4, double par6, float par8, float par9)
	{
		this.enderElfModel.isCarrying = (par1EntityEnderelf.getCarried().getMaterial() != Material.air);
		this.enderElfModel.isAttacking = par1EntityEnderelf.isScreaming();
		if (par1EntityEnderelf.isScreaming())
		{
			double var10 = 0.02D;
			par2 += this.rnd.nextGaussian() * var10;
			par6 += this.rnd.nextGaussian() * var10;
		}
		super.doRender((EntityLiving)par1EntityEnderelf, par2, par4, par6, par8, par9);
	}

	protected void renderEquippedItems(EntityEnderElf par1EntityEnderelf, float par2)
	{
		super.renderEquippedItems(par1EntityEnderelf, par2);
		if (par1EntityEnderelf.getCarried().getMaterial() != Material.air)
		{
			GL11.glEnable(GL12.GL_RESCALE_NORMAL);
			GL11.glPushMatrix();
			float f1 = 0.5F;
			GL11.glTranslatef(0.0F, 0.6875F, -0.75F);
			f1 *= 1.0F;
			GL11.glRotatef(20.0F, 1.0F, 0.0F, 0.0F);
			GL11.glRotatef(45.0F, 0.0F, 1.0F, 0.0F);
			GL11.glScalef(-f1, -f1, f1);
			int var4 = par1EntityEnderelf.getBrightnessForRender(par2);
			int var5 = var4 % 65536;
			int var6 = var4 / 65536;
			OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, var5 / 1.0F, var6 / 1.0F);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			bindTexture(TextureMap.locationBlocksTexture);
			RenderPresent.renderBlockCarried(par1EntityEnderelf.getCarried(), par1EntityEnderelf.getCarryingData(), 1.0F);
			GL11.glPopMatrix();
			GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		}
	}

	protected int renderEyes(EntityEnderElf par1EntityEnderelf, int par2, float par3)
	{
		if (par2 != 0)
		{
			return -1;
		}
		bindTexture(enderelfEyesTexture);
		float var4 = 1.0F;
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE);
		GL11.glDisable(GL11.GL_LIGHTING);
		if (par1EntityEnderelf.isInvisible())
		{
			GL11.glDepthMask(false);
		}
		else 
		{
			GL11.glDepthMask(true);
		}
		char var5 = 61680;
		int var6 = var5 % 65536;
		int var7 = var5 / 65536;
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, var6 / 1.0F, var7 / 1.0F);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, var4);
		return 1;
	}

	@Override
	public void doRender(EntityLiving par1EntityLiving, double par2, double par4, double par6, float par8, float par9)
	{
		doRender((EntityEnderElf)par1EntityLiving, par2, par4, par6, par8, par9);
	}

	@Override
	protected int inheritRenderPass(EntityLivingBase par1EntityLivingBase, int par2, float par3)
	{
		return renderEyes((EntityEnderElf)par1EntityLivingBase, par2, par3);
	}

	@Override
	protected void renderEquippedItems(EntityLivingBase par1EntityLivingBase, float par2)
	{
		renderEquippedItems((EntityEnderElf)par1EntityLivingBase, par2);
	}

	@Override
	public void doRender(EntityLivingBase par1EntityLivingBase, double par2, double par4, double par6, float par8, float par9)
	{
		doRender((EntityEnderElf)par1EntityLivingBase, par2, par4, par6, par8, par9);
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity entity)
	{
		return enderelfTextures;
	}

	@Override
	public void doRender(Entity par1Entity, double par2, double par4, double par6, float par8, float par9)
	{
		doRender((EntityEnderElf)par1Entity, par2, par4, par6, par8, par9);
	}
}