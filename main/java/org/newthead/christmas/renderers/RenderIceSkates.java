package org.newthead.christmas.renderers;

import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderPlayerEvent;

import org.lwjgl.opengl.GL11;
import org.newthead.christmas.items.ItemIceSkates;
import org.newthead.christmas.models.ModelIceSkates;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderIceSkates
{
	private ModelIceSkates iceSkatesModel = new ModelIceSkates(0.5F);
	private static final ResourceLocation iceSkatesTexture = new ResourceLocation("christmascraft", "textures/models/armor/iceskates.png");

	public RenderIceSkates()
	{
		this.iceSkatesModel.bipedHeadwear.showModel = false;
		this.iceSkatesModel.bipedBody.showModel = false;
		this.iceSkatesModel.bipedRightLeg.showModel = true;
		this.iceSkatesModel.bipedLeftLeg.showModel = true;
	}

	@SubscribeEvent
	public void onSetArmorModel(RenderPlayerEvent.SetArmorModel event)
	{
		if (event.slot == 0 && event.stack != null && event.stack.getItem() instanceof ItemIceSkates)
		{
			RenderManager.instance.renderEngine.bindTexture(iceSkatesTexture);
			event.renderer.setRenderPassModel(this.iceSkatesModel);
			GL11.glColor3f(1.0F, 1.0F, 1.0F);
			event.result = 1;
		}
	}
}
