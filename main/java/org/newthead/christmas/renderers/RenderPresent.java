package org.newthead.christmas.renderers;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;

import org.lwjgl.opengl.GL11;
import org.newthead.christmas.ModSetup;
import org.newthead.christmas.blocks.BlockChristmasPresent;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderPresent implements ISimpleBlockRenderingHandler
{
	public RenderPresent()
	{
		ModSetup.presentRenderId = RenderingRegistry.getNextAvailableRenderId();
	}

	@Override
	public void renderInventoryBlock(Block block, int metadata, int modelID, RenderBlocks renderer) {}

	public static void renderBlockCarried(Block block, int metadata, float tint)
	{
		RenderBlocks renderer = RenderBlocks.getInstance();

		BlockChristmasPresent present = (BlockChristmasPresent)block;
		Tessellator tessellator = Tessellator.instance;

		GL11.glPushMatrix();
		GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
		GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
		for (int renderPass = 1; renderPass <= 2; renderPass++)
		{
			if ((metadata != 1) || (renderPass != 2))
			{
				present.setXmasBoxBounds(metadata, renderPass);
				renderer.setRenderBoundsFromBlock(block);

				tessellator.startDrawingQuads();
				tessellator.setNormal(0.0F, -1.0F, 0.0F);
				renderer.renderFaceYNeg(block, 0.0D, 0.0D, 0.0D, renderer.getBlockIconFromSideAndMetadata(block, 0, metadata));
				tessellator.draw();

				tessellator.startDrawingQuads();
				tessellator.setNormal(0.0F, 1.0F, 0.0F);
				renderer.renderFaceYPos(block, 0.0D, 0.0D, 0.0D, renderer.getBlockIconFromSideAndMetadata(block, 1, metadata));
				tessellator.draw();

				tessellator.startDrawingQuads();
				tessellator.setNormal(0.0F, 0.0F, -1.0F);
				renderer.renderFaceZNeg(block, 0.0D, 0.0D, 0.0D, renderer.getBlockIconFromSideAndMetadata(block, 2, metadata));
				tessellator.draw();

				tessellator.startDrawingQuads();
				tessellator.setNormal(0.0F, 0.0F, 1.0F);
				renderer.renderFaceZPos(block, 0.0D, 0.0D, 0.0D, renderer.getBlockIconFromSideAndMetadata(block, 3, metadata));
				tessellator.draw();

				tessellator.startDrawingQuads();
				tessellator.setNormal(-1.0F, 0.0F, 0.0F);
				renderer.renderFaceXNeg(block, 0.0D, 0.0D, 0.0D, renderer.getBlockIconFromSideAndMetadata(block, 4, metadata));
				tessellator.draw();

				tessellator.startDrawingQuads();
				tessellator.setNormal(1.0F, 0.0F, 0.0F);
				renderer.renderFaceXPos(block, 0.0D, 0.0D, 0.0D, renderer.getBlockIconFromSideAndMetadata(block, 5, metadata));
				tessellator.draw();
			}
		}
		present.setXmasBoxBounds(metadata, 0);

		tessellator.startDrawingQuads();
		IIcon bow = present.bowIcon;
		double d3 = bow.getInterpolatedU(8 * (metadata & 0x1));
		double d4 = bow.getInterpolatedU(8 * (metadata & 0x1) + 7.99F);
		double d5 = bow.getInterpolatedV(8 * ((metadata & 0x2) >> 1));
		double d6 = bow.getInterpolatedV(8 * ((metadata & 0x2) >> 1) + 7.99F);
		double scaleY = 0.5D;
		double scale = 0.4D * scaleY;
		double d7 = 0.5D - scale;
		double d8 = 0.5D + scale;
		double yOffset = present.getBowHeight(metadata);
		double d9 = 0.5D - scale;
		double d10 = 0.5D + scale;
		tessellator.addVertexWithUV(d7, yOffset + scaleY, d9, d3, d5);
		tessellator.addVertexWithUV(d7, yOffset, d9, d3, d6);
		tessellator.addVertexWithUV(d8, yOffset, d10, d4, d6);
		tessellator.addVertexWithUV(d8, yOffset + scaleY, d10, d4, d5);
		tessellator.addVertexWithUV(d8, yOffset + scaleY, d10, d3, d5);
		tessellator.addVertexWithUV(d8, yOffset, d10, d3, d6);
		tessellator.addVertexWithUV(d7, yOffset, d9, d4, d6);
		tessellator.addVertexWithUV(d7, yOffset + scaleY, d9, d4, d5);
		tessellator.addVertexWithUV(d7, yOffset + scaleY, d10, d3, d5);
		tessellator.addVertexWithUV(d7, yOffset, d10, d3, d6);
		tessellator.addVertexWithUV(d8, yOffset, d9, d4, d6);
		tessellator.addVertexWithUV(d8, yOffset + scaleY, d9, d4, d5);
		tessellator.addVertexWithUV(d8, yOffset + scaleY, d9, d3, d5);
		tessellator.addVertexWithUV(d8, yOffset, d9, d3, d6);
		tessellator.addVertexWithUV(d7, yOffset, d10, d4, d6);
		tessellator.addVertexWithUV(d7, yOffset + scaleY, d10, d4, d5);
		tessellator.draw();

		GL11.glPopMatrix();
	}

	@Override
	public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer)
	{
		BlockChristmasPresent present = (BlockChristmasPresent)block;
		int meta = world.getBlockMetadata(x, y, z);
		Tessellator tessellator = Tessellator.instance;
		present.setXmasBoxBounds(world, x, y, z, 1);
		renderer.setRenderBoundsFromBlock(present);
		renderer.renderStandardBlock(block, x, y, z);
		if (meta != 1)
		{
			present.setXmasBoxBounds(world, x, y, z, 2);
			renderer.setRenderBoundsFromBlock(present);
			renderer.renderStandardBlock(block, x, y, z);
		}
		present.setXmasBoxBounds(world, x, y, z, 0);

		tessellator.setBrightness(block.getMixedBrightnessForBlock(world, x, y, z));
		IIcon bow = present.bowIcon;
		double d3 = bow.getInterpolatedU(8 * (meta & 0x1));
		double d4 = bow.getInterpolatedU(8 * (meta & 0x1) + 7.99F);
		double d5 = bow.getInterpolatedV(8 * ((meta & 0x2) >> 1));
		double d6 = bow.getInterpolatedV(8 * ((meta & 0x2) >> 1) + 7.99F);
		double scaleY = 0.5D;
		double scale = 0.4D * scaleY;
		double d7 = x + 0.5D - scale;
		double d8 = x + 0.5D + scale;
		double yOffset = y + present.getBowHeight(world, x, y, z);
		double d9 = z + 0.5D - scale;
		double d10 = z + 0.5D + scale;
		tessellator.addVertexWithUV(d7, yOffset + scaleY, d9, d3, d5);
		tessellator.addVertexWithUV(d7, yOffset, d9, d3, d6);
		tessellator.addVertexWithUV(d8, yOffset, d10, d4, d6);
		tessellator.addVertexWithUV(d8, yOffset + scaleY, d10, d4, d5);
		tessellator.addVertexWithUV(d8, yOffset + scaleY, d10, d3, d5);
		tessellator.addVertexWithUV(d8, yOffset, d10, d3, d6);
		tessellator.addVertexWithUV(d7, yOffset, d9, d4, d6);
		tessellator.addVertexWithUV(d7, yOffset + scaleY, d9, d4, d5);
		tessellator.addVertexWithUV(d7, yOffset + scaleY, d10, d3, d5);
		tessellator.addVertexWithUV(d7, yOffset, d10, d3, d6);
		tessellator.addVertexWithUV(d8, yOffset, d9, d4, d6);
		tessellator.addVertexWithUV(d8, yOffset + scaleY, d9, d4, d5);
		tessellator.addVertexWithUV(d8, yOffset + scaleY, d9, d3, d5);
		tessellator.addVertexWithUV(d8, yOffset, d9, d3, d6);
		tessellator.addVertexWithUV(d7, yOffset, d10, d4, d6);
		tessellator.addVertexWithUV(d7, yOffset + scaleY, d10, d4, d5);
		return true;
	}

	@Override
	public boolean shouldRender3DInInventory(int i)
	{
		return false;
	}

	@Override
	public int getRenderId()
	{
		return ModSetup.presentRenderId;
	}
}
