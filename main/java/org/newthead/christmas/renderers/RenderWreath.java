package org.newthead.christmas.renderers;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;

import org.newthead.christmas.ModSetup;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import cpw.mods.fml.client.registry.RenderingRegistry;

public class RenderWreath implements ISimpleBlockRenderingHandler
{
	public RenderWreath()
	{
		ModSetup.wreathRenderId = RenderingRegistry.getNextAvailableRenderId();
	}

	@Override
	public void renderInventoryBlock(Block block, int metadata, int modelID, RenderBlocks renderer) {}

	@Override
	public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer)
	{
		IIcon texture = renderer.getBlockIconFromSide(block, 0);
		Tessellator tessellator = Tessellator.instance;
		tessellator.setBrightness(block.getMixedBrightnessForBlock(world, x, y, z));
		float colorMul = 1.0F;
		tessellator.setColorOpaque_F(colorMul, colorMul, colorMul);
		double texX1 = texture.getMinU();
		double texX2 = texture.getMaxU();
		double texY1 = texture.getMinV();
		double texY2 = texture.getMaxV();
		int meta = world.getBlockMetadata(x, y, z);
		double offset = 0.2D;
		if (meta == 5)
		{
			tessellator.addVertexWithUV(x + offset, y + 1.0D, z + 1.0D, texX1, texY1);
			tessellator.addVertexWithUV(x, y, z + 1.0D, texX1, texY2);
			tessellator.addVertexWithUV(x, y, z, texX2, texY2);
			tessellator.addVertexWithUV(x + offset, y + 1.0D, z, texX2, texY1);
			tessellator.addVertexWithUV(x, y + 1.0D, z + 1.0D, texX1, texY1);
			tessellator.addVertexWithUV(x + offset, y, z + 1.0D, texX1, texY2);
			tessellator.addVertexWithUV(x + offset, y, z, texX2, texY2);
			tessellator.addVertexWithUV(x, y + 1.0D, z, texX2, texY1);
			tessellator.addVertexWithUV(x + offset, y + 1.0D, z + 1.0D, texX1, texY1);
			tessellator.addVertexWithUV(x + offset, y, z + 1.0D, texX1, texY2);
			tessellator.addVertexWithUV(x, y, z, texX2, texY2);
			tessellator.addVertexWithUV(x, y + 1.0D, z, texX2, texY1);
			tessellator.addVertexWithUV(x, y + 1.0D, z + 1.0D, texX1, texY1);
			tessellator.addVertexWithUV(x, y, z + 1.0D, texX1, texY2);
			tessellator.addVertexWithUV(x + offset, y, z, texX2, texY2);
			tessellator.addVertexWithUV(x + offset, y + 1.0D, z, texX2, texY1);
		}
		if (meta == 4)
		{
			tessellator.addVertexWithUV(x + 1.0D - offset, y, z + 1.0D, texX2, texY2);
			tessellator.addVertexWithUV(x + 1.0D, y + 1.0D, z + 1.0D, texX2, texY1);
			tessellator.addVertexWithUV(x + 1.0D, y + 1.0D, z, texX1, texY1);
			tessellator.addVertexWithUV(x + 1.0D - offset, y, z, texX1, texY2);
			tessellator.addVertexWithUV(x + 1.0D, y, z + 1.0D, texX2, texY2);
			tessellator.addVertexWithUV(x + 1.0D - offset, y + 1.0D, z + 1.0D, texX2, texY1);
			tessellator.addVertexWithUV(x + 1.0D - offset, y + 1.0D, z, texX1, texY1);
			tessellator.addVertexWithUV(x + 1.0D, y, z, texX1, texY2);
			tessellator.addVertexWithUV(x + 1.0D - offset, y, z + 1.0D, texX2, texY2);
			tessellator.addVertexWithUV(x + 1.0D - offset, y + 1.0D, z + 1.0D, texX2, texY1);
			tessellator.addVertexWithUV(x + 1.0D, y + 1.0D, z, texX1, texY1);
			tessellator.addVertexWithUV(x + 1.0D, y, z, texX1, texY2);
			tessellator.addVertexWithUV(x + 1.0D, y, z + 1.0D, texX2, texY2);
			tessellator.addVertexWithUV(x + 1.0D, y + 1.0D, z + 1.0D, texX2, texY1);
			tessellator.addVertexWithUV(x + 1.0D - offset, y + 1.0D, z, texX1, texY1);
			tessellator.addVertexWithUV(x + 1.0D - offset, y, z, texX1, texY2);
		}
		if (meta == 3)
		{
			tessellator.addVertexWithUV(x + 1.0D, y, z + offset, texX2, texY2);
			tessellator.addVertexWithUV(x + 1.0D, y + 1.0D, z, texX2, texY1);
			tessellator.addVertexWithUV(x, y + 1.0D, z, texX1, texY1);
			tessellator.addVertexWithUV(x, y, z + offset, texX1, texY2);
			tessellator.addVertexWithUV(x + 1.0D, y, z, texX2, texY2);
			tessellator.addVertexWithUV(x + 1.0D, y + 1.0D, z + offset, texX2, texY1);
			tessellator.addVertexWithUV(x, y + 1.0D, z + offset, texX1, texY1);
			tessellator.addVertexWithUV(x, y, z, texX1, texY2);
			tessellator.addVertexWithUV(x + 1.0D, y, z + offset, texX2, texY2);
			tessellator.addVertexWithUV(x + 1.0D, y + 1.0D, z + offset, texX2, texY1);
			tessellator.addVertexWithUV(x, y + 1.0D, z, texX1, texY1);
			tessellator.addVertexWithUV(x, y, z, texX1, texY2);
			tessellator.addVertexWithUV(x + 1.0D, y, z, texX2, texY2);
			tessellator.addVertexWithUV(x + 1.0D, y + 1.0D, z, texX2, texY1);
			tessellator.addVertexWithUV(x, y + 1.0D, z + offset, texX1, texY1);
			tessellator.addVertexWithUV(x, y, z + offset, texX1, texY2);
		}
		if (meta == 2)
		{
			tessellator.addVertexWithUV(x + 1.0D, y + 1.0D, z + 1.0D - offset, texX1, texY1);
			tessellator.addVertexWithUV(x + 1.0D, y, z + 1.0D, texX1, texY2);
			tessellator.addVertexWithUV(x, y, z + 1.0D, texX2, texY2);
			tessellator.addVertexWithUV(x, y + 1.0D, z + 1.0D - offset, texX2, texY1);
			tessellator.addVertexWithUV(x + 1.0D, y + 1.0D, z + 1.0D, texX1, texY1);
			tessellator.addVertexWithUV(x + 1.0D, y, z + 1.0D - offset, texX1, texY2);
			tessellator.addVertexWithUV(x, y, z + 1.0D - offset, texX2, texY2);
			tessellator.addVertexWithUV(x, y + 1.0D, z + 1.0D, texX2, texY1);
			tessellator.addVertexWithUV(x + 1.0D, y + 1.0D, z + 1.0D - offset, texX1, texY1);
			tessellator.addVertexWithUV(x + 1.0D, y, z + 1.0D - offset, texX1, texY2);
			tessellator.addVertexWithUV(x, y, z + 1.0D, texX2, texY2);
			tessellator.addVertexWithUV(x, y + 1.0D, z + 1.0D, texX2, texY1);
			tessellator.addVertexWithUV(x + 1.0D, y + 1.0D, z + 1.0D, texX1, texY1);
			tessellator.addVertexWithUV(x + 1.0D, y, z + 1.0D, texX1, texY2);
			tessellator.addVertexWithUV(x, y, z + 1.0D - offset, texX2, texY2);
			tessellator.addVertexWithUV(x, y + 1.0D, z + 1.0D - offset, texX2, texY1);
		}
		return true;
	}

	@Override
	public boolean shouldRender3DInInventory(int i)
	{
		return false;
	}

	@Override
	public int getRenderId()
	{
		return ModSetup.wreathRenderId;
	}
}
