package org.newthead.christmas.renderers;

import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

import org.newthead.christmas.models.ModelCreeperClaus;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderCreeperClaus extends RenderLiving
{
	private static final ResourceLocation creeperClausTexture = new ResourceLocation("christmascraft", "textures/models/creeperclaus.png");

	public RenderCreeperClaus()
	{
		super(new ModelCreeperClaus(), 0.5F);
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity entity)
	{
		return creeperClausTexture;
	}
}
