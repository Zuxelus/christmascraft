package org.newthead.christmas.renderers;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;

import org.newthead.christmas.ModSetup;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderChristmasLight implements ISimpleBlockRenderingHandler
{
	public RenderChristmasLight()
	{
		ModSetup.christmasLightRenderId = RenderingRegistry.getNextAvailableRenderId();
	}

	@Override
	public void renderInventoryBlock(Block block, int metadata, int modelID, RenderBlocks renderer) {}

	@Override
	public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer)
	{
		IIcon texture = renderer.getBlockIcon(block, world, x, y, z, 0);
		Tessellator tessellator = Tessellator.instance;
		float colorMul = 1.0F;
		tessellator.setBrightness(block.getMixedBrightnessForBlock(world, x, y, z));
		int color = block.colorMultiplier(world, x, y, z);
		float colorR = (color >> 16 & 0xFF) / 255.0F;
		float colorG = (color >> 8 & 0xFF) / 255.0F;
		float colorB = (color & 0xFF) / 255.0F;
		tessellator.setColorOpaque_F(colorMul * colorR, colorMul * colorG, colorMul * colorB);

		double texX1 = texture.getMinU();
		double texX2 = texture.getMaxU();
		double texY1 = texture.getMinV();
		double texY2 = texture.getMaxV();
		double offset = 0.05D;
		Block neighbor = world.getBlock(x - 1, y, z);
		if (neighbor.renderAsNormalBlock() && neighbor.getMaterial().isSolid())
		{
			tessellator.addVertexWithUV(x + offset, y + 1, z + 1, texX1, texY1);
			tessellator.addVertexWithUV(x + offset, y + 0, z + 1, texX1, texY2);
			tessellator.addVertexWithUV(x + offset, y + 0, z + 0, texX2, texY2);
			tessellator.addVertexWithUV(x + offset, y + 1, z + 0, texX2, texY1);
			tessellator.addVertexWithUV(x + offset, y + 1, z + 0, texX2, texY1);
			tessellator.addVertexWithUV(x + offset, y + 0, z + 0, texX2, texY2);
			tessellator.addVertexWithUV(x + offset, y + 0, z + 1, texX1, texY2);
			tessellator.addVertexWithUV(x + offset, y + 1, z + 1, texX1, texY1);
		}
		neighbor = world.getBlock(x + 1, y, z);
		if (neighbor.renderAsNormalBlock() && neighbor.getMaterial().isSolid())
		{
			tessellator.addVertexWithUV(x + 1 - offset, y + 0, z + 1, texX2, texY2);
			tessellator.addVertexWithUV(x + 1 - offset, y + 1, z + 1, texX2, texY1);
			tessellator.addVertexWithUV(x + 1 - offset, y + 1, z + 0, texX1, texY1);
			tessellator.addVertexWithUV(x + 1 - offset, y + 0, z + 0, texX1, texY2);
			tessellator.addVertexWithUV(x + 1 - offset, y + 0, z + 0, texX1, texY2);
			tessellator.addVertexWithUV(x + 1 - offset, y + 1, z + 0, texX1, texY1);
			tessellator.addVertexWithUV(x + 1 - offset, y + 1, z + 1, texX2, texY1);
			tessellator.addVertexWithUV(x + 1 - offset, y + 0, z + 1, texX2, texY2);
		}
		neighbor = world.getBlock(x, y, z - 1);
		if (neighbor.renderAsNormalBlock() && neighbor.getMaterial().isSolid())
		{
			tessellator.addVertexWithUV(x + 1, y + 0, z + offset, texX2, texY2);
			tessellator.addVertexWithUV(x + 1, y + 1, z + offset, texX2, texY1);
			tessellator.addVertexWithUV(x + 0, y + 1, z + offset, texX1, texY1);
			tessellator.addVertexWithUV(x + 0, y + 0, z + offset, texX1, texY2);
			tessellator.addVertexWithUV(x + 0, y + 0, z + offset, texX1, texY2);
			tessellator.addVertexWithUV(x + 0, y + 1, z + offset, texX1, texY1);
			tessellator.addVertexWithUV(x + 1, y + 1, z + offset, texX2, texY1);
			tessellator.addVertexWithUV(x + 1, y + 0, z + offset, texX2, texY2);
		}
		neighbor = world.getBlock(x, y, z + 1);
		if (neighbor.renderAsNormalBlock() && neighbor.getMaterial().isSolid())
		{
			tessellator.addVertexWithUV(x + 1, y + 1, z + 1 - offset, texX1, texY1);
			tessellator.addVertexWithUV(x + 1, y + 0, z + 1 - offset, texX1, texY2);
			tessellator.addVertexWithUV(x + 0, y + 0, z + 1 - offset, texX2, texY2);
			tessellator.addVertexWithUV(x + 0, y + 1, z + 1 - offset, texX2, texY1);
			tessellator.addVertexWithUV(x + 0, y + 1, z + 1 - offset, texX2, texY1);
			tessellator.addVertexWithUV(x + 0, y + 0, z + 1 - offset, texX2, texY2);
			tessellator.addVertexWithUV(x + 1, y + 0, z + 1 - offset, texX1, texY2);
			tessellator.addVertexWithUV(x + 1, y + 1, z + 1 - offset, texX1, texY1);
		}
		return true;
	}

	@Override
	public boolean shouldRender3DInInventory(int i)
	{
		return false;
	}

	@Override
	public int getRenderId()
	{
		return ModSetup.christmasLightRenderId;
	}
}
