package org.newthead.christmas.renderers;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;

import org.newthead.christmas.ModSetup;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderStocking implements ISimpleBlockRenderingHandler
{
	public RenderStocking()
	{
		ModSetup.stockingRenderId = RenderingRegistry.getNextAvailableRenderId();
	}

	@Override
	public void renderInventoryBlock(Block block, int metadata, int modelID, RenderBlocks renderer) {}

	@Override
	public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer)
	{
		Tessellator tessellator = Tessellator.instance;
		int k1 = world.getBlockMetadata(x, y, z);
		IIcon texture = renderer.getBlockIconFromSideAndMetadata(block, 0, k1);
		k1 &= 0x3;
		tessellator.setBrightness(block.getMixedBrightnessForBlock(world, x, y, z));
		float f = 1.0F;
		tessellator.setColorOpaque_F(f, f, f);
		double texX1 = texture.getMinU();
		double texX2 = texture.getMaxU();
		double texY1 = texture.getMinV();
		double texY2 = texture.getMaxV();
		double d4 = 0.0D;
		double d5 = 0.05D;
		if (k1 == 3)
		{
			tessellator.addVertexWithUV(x + d5, y + 1 + d4, z + 1 + d4, texX1, texY1);
			tessellator.addVertexWithUV(x + d5, y + 0 - d4, z + 1 + d4, texX1, texY2);
			tessellator.addVertexWithUV(x + d5, y + 0 - d4, z + 0 - d4, texX2, texY2);
			tessellator.addVertexWithUV(x + d5, y + 1 + d4, z + 0 - d4, texX2, texY1);
		}
		if (k1 == 2)
		{
			tessellator.addVertexWithUV(x + 1 - d5, y + 0 - d4, z + 1 + d4, texX2, texY2);
			tessellator.addVertexWithUV(x + 1 - d5, y + 1 + d4, z + 1 + d4, texX2, texY1);
			tessellator.addVertexWithUV(x + 1 - d5, y + 1 + d4, z + 0 - d4, texX1, texY1);
			tessellator.addVertexWithUV(x + 1 - d5, y + 0 - d4, z + 0 - d4, texX1, texY2);
		}
		if (k1 == 1)
		{
			tessellator.addVertexWithUV(x + 1 + d4, y + 0 - d4, z + d5, texX2, texY2);
			tessellator.addVertexWithUV(x + 1 + d4, y + 1 + d4, z + d5, texX2, texY1);
			tessellator.addVertexWithUV(x + 0 - d4, y + 1 + d4, z + d5, texX1, texY1);
			tessellator.addVertexWithUV(x + 0 - d4, y + 0 - d4, z + d5, texX1, texY2);
		}
		if (k1 == 0)
		{
			tessellator.addVertexWithUV(x + 1 + d4, y + 1 + d4, z + 1 - d5, texX1, texY1);
			tessellator.addVertexWithUV(x + 1 + d4, y + 0 - d4, z + 1 - d5, texX1, texY2);
			tessellator.addVertexWithUV(x + 0 - d4, y + 0 - d4, z + 1 - d5, texX2, texY2);
			tessellator.addVertexWithUV(x + 0 - d4, y + 1 + d4, z + 1 - d5, texX2, texY1);
		}
		return true;
	}

	@Override
	public boolean shouldRender3DInInventory(int i)
	{
		return false;
	}

	@Override
	public int getRenderId()
	{
		return ModSetup.stockingRenderId;
	}
}
