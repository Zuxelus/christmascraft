package org.newthead.christmas.renderers;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;

import org.newthead.christmas.ModSetup;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderTreestand implements ISimpleBlockRenderingHandler
{
	public RenderTreestand()
	{
		ModSetup.treestandRenderId = RenderingRegistry.getNextAvailableRenderId();
	}

	@Override
	public void renderInventoryBlock(Block block, int metadata, int modelID, RenderBlocks renderer) {}

	@Override
	public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer)
	{
		Tessellator tessellator = Tessellator.instance;
		tessellator.setBrightness(block.getMixedBrightnessForBlock(world, x, y, z));
		float f = 1.0F;
		float offset1 = 0.1875F;
		float offset2 = 1.0F - offset1;
		float offsetY = 0.375F;
		tessellator.setColorOpaque_F(f, f, f);
		IIcon texture = renderer.getBlockIconFromSide(block, 0);
		double texX1 = texture.getMinU();
		double texX2 = texture.getMaxU();
		double texY1 = texture.getMinV();
		double texY2 = texture.getMaxV();
		tessellator.addVertexWithUV(x + offset1, y + offsetY, z + offset1, texX1, texY1);
		tessellator.addVertexWithUV(x + offset2, y + offsetY, z + offset1, texX1, texY2);
		tessellator.addVertexWithUV(x + offset2, y + offsetY, z + offset2, texX2, texY2);
		tessellator.addVertexWithUV(x + offset1, y + offsetY, z + offset2, texX2, texY1);

		texture = renderer.getBlockIconFromSide(block, 1);
		texX1 = texture.getMinU();
		texX2 = texture.getMaxU();
		texY1 = texture.getMinV();
		texY2 = texture.getMaxV();
		tessellator.addVertexWithUV(x, y + 1.0D, z, texX1, texY1);
		tessellator.addVertexWithUV(x, y + 1.0D, z + 1.0D, texX1, texY2);
		tessellator.addVertexWithUV(x + 1.0D, y + 1.0D, z + 1.0D, texX2, texY2);
		tessellator.addVertexWithUV(x + 1.0D, y + 1.0D, z, texX2, texY1);

		texture = renderer.getBlockIconFromSide(block, 2);
		texX1 = texture.getMinU();
		texX2 = texture.getMaxU();
		texY1 = texture.getMinV();
		texY2 = texture.getInterpolatedV(16.0F * (1.0F - offsetY));
		tessellator.addVertexWithUV(x + 1.0D, y + 1.0D, z, texX1, texY1);
		tessellator.addVertexWithUV(x + 1.0D, y + offsetY, z + offset1, texX1, texY2);
		tessellator.addVertexWithUV(x, y + offsetY, z + offset1, texX2, texY2);
		tessellator.addVertexWithUV(x, y + 1.0D, z, texX2, texY1);

		texture = renderer.getBlockIconFromSide(block, 3);
		texX1 = texture.getMinU();
		texX2 = texture.getMaxU();
		texY1 = texture.getMinV();
		texY2 = texture.getInterpolatedV(16.0F * (1.0F - offsetY));
		tessellator.addVertexWithUV(x, y + 1.0D, z + 1.0D, texX1, texY1);
		tessellator.addVertexWithUV(x, y + offsetY, z + offset2, texX1, texY2);
		tessellator.addVertexWithUV(x + 1.0D, y + offsetY, z + offset2, texX2, texY2);
		tessellator.addVertexWithUV(x + 1.0D, y + 1.0D, z + 1.0D, texX2, texY1);

		texture = renderer.getBlockIconFromSide(block, 4);
		texX1 = texture.getMinU();
		texX2 = texture.getMaxU();
		texY1 = texture.getMinV();
		texY2 = texture.getInterpolatedV(16.0F * (1.0F - offsetY));
		tessellator.addVertexWithUV(x, y + 1.0D, z, texX1, texY1);
		tessellator.addVertexWithUV(x + offset1, y + offsetY, z, texX1, texY2);
		tessellator.addVertexWithUV(x + offset1, y + offsetY, z + 1.0D, texX2, texY2);
		tessellator.addVertexWithUV(x, y + 1.0D, z + 1.0D, texX2, texY1);

		texture = renderer.getBlockIconFromSide(block, 5);
		texX1 = texture.getMinU();
		texX2 = texture.getMaxU();
		texY1 = texture.getMinV();
		texY2 = texture.getInterpolatedV(16.0F * (1.0F - offsetY));
		tessellator.addVertexWithUV(x + 1.0D, y + 1.0D, z + 1.0D, texX1, texY1);
		tessellator.addVertexWithUV(x + offset2, y + offsetY, z + 1.0D, texX1, texY2);
		tessellator.addVertexWithUV(x + offset2, y + offsetY, z, texX2, texY2);
		tessellator.addVertexWithUV(x + 1.0D, y + 1.0D, z, texX2, texY1);

		//renderer.drawCrossedSquares(block, 2, x, y, z, 1.0F);

		IIcon iicon = renderer.getBlockIconFromSideAndMetadata(block, 0, 2);
		renderer.drawCrossedSquares(iicon, x, y, z, 1.0F);

		return true;
	}

	@Override
	public boolean shouldRender3DInInventory(int i)
	{
		return false;
	}

	@Override
	public int getRenderId()
	{
		return ModSetup.treestandRenderId;
	}
}
