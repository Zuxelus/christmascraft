package org.newthead.christmas.renderers;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.IIcon;
import net.minecraft.util.Vec3;
import net.minecraft.world.IBlockAccess;

import org.newthead.christmas.ModSetup;
import org.newthead.christmas.blocks.BlockOrnament;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderOrnament implements ISimpleBlockRenderingHandler
{
	public RenderOrnament()
	{
		ModSetup.ornamentRenderId = RenderingRegistry.getNextAvailableRenderId();
	}

	@Override
	public void renderInventoryBlock(Block block, int metadata, int modelID, RenderBlocks renderer) {}

	@Override
	public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer)
	{
		BlockOrnament ornament = (BlockOrnament)block;
		Tessellator tessellator = Tessellator.instance;
		Vec3 offset = ornament.getBlockOffsets(world, x, y, z);
		offset = offset.addVector(x, y + 0.5D, z);
		int meta = world.getBlockMetadata(x, y, z);
		tessellator.setBrightness(block.getMixedBrightnessForBlock(world, x, y, z));
		float brightness = 0.9F;
		int color = block.colorMultiplier(world, x, y, z);
		float colorR = (color >> 16 & 0xFF) / 255.0F;
		float colorG = (color >> 8 & 0xFF) / 255.0F;
		float colorB = (color & 0xFF) / 255.0F;
		tessellator.setColorOpaque_F(brightness * colorR, brightness * colorG, brightness * colorB);
		IIcon texture = renderer.getBlockIconFromSideAndMetadata(block, 0, meta);
		double d3 = texture.getMinU();
		double d4 = texture.getMaxU();
		double d5 = texture.getMinV();
		double d6 = texture.getMaxV();
		double scaleY = 0.35D;
		double scale = 0.85D * scaleY;
		double d7 = offset.xCoord + 0.5D - scale;
		double d8 = offset.xCoord + 0.5D + scale;
		double d9 = offset.zCoord + 0.5D - scale;
		double d10 = offset.zCoord + 0.5D + scale;
		tessellator.addVertexWithUV(d7, offset.yCoord + scaleY, d9, d3, d5);
		tessellator.addVertexWithUV(d7, offset.yCoord - scaleY, d9, d3, d6);
		tessellator.addVertexWithUV(d8, offset.yCoord - scaleY, d10, d4, d6);
		tessellator.addVertexWithUV(d8, offset.yCoord + scaleY, d10, d4, d5);
		tessellator.addVertexWithUV(d8, offset.yCoord + scaleY, d10, d3, d5);
		tessellator.addVertexWithUV(d8, offset.yCoord - scaleY, d10, d3, d6);
		tessellator.addVertexWithUV(d7, offset.yCoord - scaleY, d9, d4, d6);
		tessellator.addVertexWithUV(d7, offset.yCoord + scaleY, d9, d4, d5);
		tessellator.addVertexWithUV(d7, offset.yCoord + scaleY, d10, d3, d5);
		tessellator.addVertexWithUV(d7, offset.yCoord - scaleY, d10, d3, d6);
		tessellator.addVertexWithUV(d8, offset.yCoord - scaleY, d9, d4, d6);
		tessellator.addVertexWithUV(d8, offset.yCoord + scaleY, d9, d4, d5);
		tessellator.addVertexWithUV(d8, offset.yCoord + scaleY, d9, d3, d5);
		tessellator.addVertexWithUV(d8, offset.yCoord - scaleY, d9, d3, d6);
		tessellator.addVertexWithUV(d7, offset.yCoord - scaleY, d10, d4, d6);
		tessellator.addVertexWithUV(d7, offset.yCoord + scaleY, d10, d4, d5);
		return true;
	}

	@Override
	public boolean shouldRender3DInInventory(int i)
	{
		return false;
	}

	@Override
	public int getRenderId()
	{
		return ModSetup.ornamentRenderId;
	}
}
