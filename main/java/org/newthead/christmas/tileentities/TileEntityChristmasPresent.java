package org.newthead.christmas.tileentities;

public class TileEntityChristmasPresent extends TileEntityStocking
{
	public void onContentsChanged()
	{
		this.blockMetadata = (this.worldObj.getBlockMetadata(this.xCoord, this.yCoord, this.zCoord) & 0x3);
		if (this.contents != null)
		{
			int tex = this.contents.getItemDamage();
			this.blockMetadata = tex;
		}
		this.worldObj.setBlockMetadataWithNotify(this.xCoord, this.yCoord, this.zCoord, this.blockMetadata, 3);
	}
}
