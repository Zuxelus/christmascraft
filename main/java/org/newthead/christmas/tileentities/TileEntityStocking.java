package org.newthead.christmas.tileentities;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;

public class TileEntityStocking extends TileEntity
{
	protected ItemStack contents;

	public ItemStack getContents()
	{
		return this.contents;
	}

	public void setContents(ItemStack itemstack)
	{
		if (itemstack != null && itemstack.stackSize > 1)
		{
			itemstack.stackSize = 1;
		}
		this.contents = itemstack;
		onContentsChanged();
	}

	public void onContentsChanged()
	{
		this.blockMetadata = (this.worldObj.getBlockMetadata(this.xCoord, this.yCoord, this.zCoord) & 0x3);
		if (this.contents != null)
		{
			int tex = this.contents.getItemDamage();
			if (tex == 0)
			{
				tex = 3;
			}
			this.blockMetadata |= tex << 2;
		}
		this.worldObj.setBlockMetadataWithNotify(this.xCoord, this.yCoord, this.zCoord, this.blockMetadata, 3);
	}

	@Override
	public void readFromNBT(NBTTagCompound nbttagcompound)
	{
		super.readFromNBT(nbttagcompound);
		NBTTagList nbttaglist = nbttagcompound.getTagList("Contents",10);
		for (int i = 0; i < nbttaglist.tagCount(); i++)
		{
			NBTTagCompound nbttagcompound1 = (NBTTagCompound)nbttaglist.getCompoundTagAt(i);
			this.contents = ItemStack.loadItemStackFromNBT(nbttagcompound1);
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound nbttagcompound)
	{
		super.writeToNBT(nbttagcompound);
		NBTTagList nbttaglist = new NBTTagList();
		if (this.contents != null)
		{
			NBTTagCompound nbttagcompound1 = new NBTTagCompound();
			this.contents.writeToNBT(nbttagcompound1);
			nbttaglist.appendTag(nbttagcompound1);
		}
		nbttagcompound.setTag("Contents", nbttaglist);
	}
}
