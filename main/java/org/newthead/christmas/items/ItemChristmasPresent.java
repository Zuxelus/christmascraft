package org.newthead.christmas.items;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

import org.newthead.christmas.ChristmasCraft;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemChristmasPresent extends ItemBlock
{
	@SideOnly(Side.CLIENT)
	private IIcon[] icons;

	public ItemChristmasPresent(Block block)
	{
		super(block);
		setMaxStackSize(1);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister par1IconRegister)
	{
		int types = 4;
		this.icons = new IIcon[types];
		String s = ChristmasCraft.blockPresent.getItemIconName();
		for (int i = 0; i < types; i++)
		{
			this.icons[i] = par1IconRegister.registerIcon(s + "_" + i);
		}
	}

	@Override
	public IIcon getIconFromDamage(int par1)
	{
		return this.icons[(par1 % 4)];
	}

	@Override
	public EnumRarity getRarity(ItemStack itemstack)
	{
		return EnumRarity.rare;
	}

	@Override
	public int getMetadata(int damage)
	{
		return damage;
	}
}
