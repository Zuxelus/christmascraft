package org.newthead.christmas.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import cpw.mods.fml.common.registry.GameRegistry;

public class ItemChristmasDrink extends ItemFood
{
	public ItemChristmasDrink(String name, int heal, float saturation)
	{
		super(heal, saturation, false);
		if (name == "eggnog")
		{
			setAlwaysEdible();
			setPotionEffect(Potion.confusion.id, 20, 0, 0.4F);
		}
		setUnlocalizedName(name);
		setTextureName("christmascraft:" + name);
		setCreativeTab(CreativeTabs.tabFood);
		GameRegistry.registerItem(this, name);
	}

	@Override
	public EnumAction getItemUseAction(ItemStack itemstack)
	{
		return EnumAction.drink;
	}
}
