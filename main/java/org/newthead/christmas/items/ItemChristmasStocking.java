package org.newthead.christmas.items;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.IIcon;

import org.newthead.christmas.ChristmasCraft;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemChristmasStocking extends ItemBlock
{
	public ItemChristmasStocking(Block block)
	{
		super(block);
		setMaxStackSize(1);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconFromDamage(int par1)
	{
		return ChristmasCraft.blockStocking.getIcon(0, par1 << 2);
	}
}
