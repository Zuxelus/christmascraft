package org.newthead.christmas.items;

import java.util.UUID;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import cpw.mods.fml.common.registry.GameRegistry;

public class ItemIceSkates extends Item
{
	private static final UUID skatingSpeedBoostModifierUUID = UUID.fromString("FE21BA1E-C395-4E03-8173-F023ABB7ACD8");
	private static final UUID skatingSpeedSlowModifierUUID = UUID.fromString("2BCB1346-8A2F-4420-B39F-4B1E7C3D7DAB");
	private static final AttributeModifier skatingSpeedBoostModifier = new AttributeModifier(skatingSpeedBoostModifierUUID, "Skating speed boost", 0.6D, 2).setSaved(false);
	private static final AttributeModifier skatingSpeedSlowModifier = new AttributeModifier(skatingSpeedSlowModifierUUID, "Skating speed slow", -0.5D, 2).setSaved(false);

	public ItemIceSkates(String name)
	{
		super();
		setUnlocalizedName(name);
		setTextureName("christmascraft:" + name);
		setCreativeTab(CreativeTabs.tabTransport);
		GameRegistry.registerItem(this, name);
	}

	@Override
	public boolean isValidArmor(ItemStack stack, int armorType, Entity entity)
	{
		if (armorType == 3 && entity instanceof EntityPlayer)
		{
			return true;
		}
		return false;
	}

	@Override
	public void onUpdate(ItemStack itemStack, World world, Entity entity, int par4, boolean par5)
	{
		if (!world.isRemote && entity instanceof EntityPlayer)
		{
			updateIceSpeedBoost((EntityPlayer)entity);
		}
	}

	@Override
	public void onArmorTick(World world, EntityPlayer player, ItemStack itemStack)
	{
		if (!world.isRemote)
		{
			updateIceSpeedBoost(player);
		}
	}

	@Override
	public boolean onDroppedByPlayer(ItemStack item, EntityPlayer player)
	{
		if (!player.worldObj.isRemote)
		{
			IAttributeInstance walkSpeed = player.getEntityAttribute(SharedMonsterAttributes.movementSpeed);
			walkSpeed.removeModifier(skatingSpeedBoostModifier);
		}
		return true;
	}

	private void updateIceSpeedBoost(EntityPlayer player)
	{
		IAttributeInstance walkSpeed = player.getEntityAttribute(SharedMonsterAttributes.movementSpeed);
		AttributeModifier existingSpeedBoost = walkSpeed.getModifier(skatingSpeedBoostModifier.getID());
		AttributeModifier existingSpeedSlow = walkSpeed.getModifier(skatingSpeedSlowModifier.getID());

		boolean applyBoost = existingSpeedBoost != null;
		boolean applySlow = existingSpeedSlow != null;

		ItemStack boots = player.getCurrentArmor(0);
		if (boots != null && boots.getItem() == this)
		{
			World world = player.getEntityWorld();
			if (world != null && player.onGround)
			{
				Block block = world.getBlock(MathHelper.floor_double(player.posX), MathHelper.floor_double(player.boundingBox.minY) - 1, MathHelper.floor_double(player.posZ));
				if (block == Blocks.ice)
				{
					applyBoost = true;
					applySlow = false;
				}
				else if (block != null)
				{
					applyBoost = false;
					applySlow = true;
				}
			}
		}
		else
		{
			applyBoost = false;
			applySlow = false;
		}
		if (applyBoost && existingSpeedBoost == null)
		{
			walkSpeed.applyModifier(skatingSpeedBoostModifier);
		}
		else if (!applyBoost && existingSpeedBoost != null)
		{
			walkSpeed.removeModifier(skatingSpeedBoostModifier);
		}
		if (applySlow && existingSpeedSlow == null)
		{
			walkSpeed.applyModifier(skatingSpeedSlowModifier);
		}
		else if (!applySlow && existingSpeedSlow != null)
		{
			walkSpeed.removeModifier(skatingSpeedSlowModifier);
		}
	}
}
