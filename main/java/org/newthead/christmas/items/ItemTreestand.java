package org.newthead.christmas.items;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import org.newthead.christmas.ChristmasCraft;

public class ItemTreestand extends ItemBlock
{
	private Block spawn = ChristmasCraft.blockTreestand;

	public ItemTreestand(Block block)
	{
		super(block);
	}

	@Override
	public boolean onItemUse(ItemStack itemStack, EntityPlayer entity, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ)
	{
		Block clicked = world.getBlock(x, y, z);
		if (clicked == Blocks.snow)
		{
			side = 1;
		}
		else if (clicked != Blocks.vine && clicked != Blocks.tallgrass && clicked != Blocks.deadbush)
		{
			if (side == 0) {
				y--;
			} else if (side == 1) {
				y++;
			} else if (side == 2) {
				z--;
			} else if (side == 3) {
				z++;
			} else if (side == 4) {
				x--;
			} else if (side == 5) {
				x++;
			}
		}
		if (!world.doesBlockHaveSolidTopSurface(world, x, y - 1, z))
		{
			return false;
		}
		if (!entity.canPlayerEdit(x, y, z, side, itemStack))
		{
			return false;
		}
		if (itemStack.stackSize == 0)
		{
			return false;
		}
		if (world.canPlaceEntityOnSide(this.spawn, x, y, z, false, side, entity, itemStack))
		{
			Block block = this.spawn;
			int metadata = block.onBlockPlaced(world, x, y, z, side, hitX, hitY, hitZ, 0);
			if (world.setBlock(x, y, z, this.spawn, metadata, 3) && world.getBlock(x, y, z) == this.spawn)
			{
				this.spawn.onBlockPlacedBy(world, x, y, z, entity, itemStack);
				this.spawn.onPostBlockPlaced(world, x, y, z, metadata);

				world.playSoundEffect(x + 0.5D, y + 0.5D, z + 0.5D, block.stepSound.getStepResourcePath(), (block.stepSound.getVolume() + 1.0F) / 2.0F, block.stepSound.getPitch() * 0.8F);
				itemStack.stackSize -= 1;
			}
			return true;
		}
		return false;
	}
}
