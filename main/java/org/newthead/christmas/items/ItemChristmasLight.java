package org.newthead.christmas.items;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;

import org.newthead.christmas.ChristmasCraft;
import org.newthead.christmas.blocks.BlockChristmasLight;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemChristmasLight extends ItemBlock
{
	public ItemChristmasLight(Block block)
	{
		super(block);
		setMaxDamage(0);
		setHasSubtypes(true);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconFromDamage(int par1)
	{
		return ChristmasCraft.blockChristmasLightOff.getIcon(2, par1);
	}

	@Override
	public int getMetadata(int damage)
	{
		return damage;
	}

	@Override
	public String getUnlocalizedName(ItemStack stack)
	{
		return getUnlocalizedName() + "." + BlockChristmasLight.names[MathHelper.clamp_int(stack.getItemDamage(), 0, 5)];
	}
}
