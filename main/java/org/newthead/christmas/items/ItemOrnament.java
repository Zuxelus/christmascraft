package org.newthead.christmas.items;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

import org.newthead.christmas.ChristmasCraft;
import org.newthead.christmas.blocks.BlockOrnament;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemOrnament extends ItemBlock
{
	public ItemOrnament(Block block)
	{
		super(block);
		setMaxDamage(0);
		setHasSubtypes(true);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconFromDamage(int par1)
	{
		return ChristmasCraft.blockOrnament.getIcon(0, par1);
	}

	@Override
	public int getMetadata(int damage)
	{
		return damage;
	}

	@Override
	public String getUnlocalizedName(ItemStack par1ItemStack)
	{
		return getUnlocalizedName() + "." + BlockOrnament.names[par1ItemStack.getItemDamage()];
	}
}
