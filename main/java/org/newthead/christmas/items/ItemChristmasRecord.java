package org.newthead.christmas.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemRecord;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemChristmasRecord extends ItemRecord
{
	public ItemChristmasRecord(String name)
	{
		super(name);
		setUnlocalizedName("record");
		setTextureName("christmascraft:record_" + name);
		setCreativeTab(CreativeTabs.tabDecorations);
		GameRegistry.registerItem(this, name);
	}

	@Override
    public ResourceLocation getRecordResource(String name)
    {
        return new ResourceLocation("christmascraft",name.split("\\.")[1]);
    }
    
	@Override
	@SideOnly(Side.CLIENT)
	public String getRecordNameLocal()
	{
		return StatCollector.translateToLocal("music.christmas." + this.recordName + ".name");
	}
}
