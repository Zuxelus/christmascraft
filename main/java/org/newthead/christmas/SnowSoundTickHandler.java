package org.newthead.christmas;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MathHelper;
import net.minecraft.world.biome.BiomeGenBase;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent.ClientTickEvent;
import cpw.mods.fml.common.gameevent.TickEvent.Phase;

public class SnowSoundTickHandler
{
	public final static SnowSoundTickHandler instance = new SnowSoundTickHandler();
	private Random random = new Random();
	int rendererUpdateCount;
	int snowSoundCounter;

	@SubscribeEvent
	public void onClientTick(ClientTickEvent event)
	{
		if (event.phase == Phase.START)
		{
			if (!ChristmasCraft.isChristmasTime() || !ChristmasCraft.shouldPlaySnowSound())
			{
				return;
			}
			Minecraft mc = Minecraft.getMinecraft();
			WorldClient world = mc.theWorld;
			if (world == null)
			{
				return;
			}
			float rainStrength = world.getRainStrength(1.0F);
			this.rendererUpdateCount += 1;
			if (!mc.gameSettings.fancyGraphics)
			{
				rainStrength /= 2.0F;
			}
			if (rainStrength == 0.0F)
			{
				return;
			}
			this.random.setSeed(this.rendererUpdateCount * 312987231L);
			EntityLivingBase cam = mc.renderViewEntity;
			int x = MathHelper.floor_double(cam.posX);
			int y = MathHelper.floor_double(cam.posY);
			int z = MathHelper.floor_double(cam.posZ);
			byte renderDistance = 10;
			double d = 0.0D;
			double d1 = 0.0D;
			double d2 = 0.0D;
			int l = 0;
			int particles = (int)(100.0F * rainStrength * rainStrength);
			if (mc.gameSettings.particleSetting == 1)
			{
				particles >>= 1;
			}
			else if (mc.gameSettings.particleSetting == 2)
			{
				particles = 0;
			}
			for (int p = 0; p < particles; p++)
			{
				int randX = x + this.random.nextInt(renderDistance) - this.random.nextInt(renderDistance);
				int randZ = z + this.random.nextInt(renderDistance) - this.random.nextInt(renderDistance);
				int rainHeight = world.getPrecipitationHeight(randX, randZ);
				Block block = world.getBlock(randX, rainHeight - 1, randZ);
				BiomeGenBase biome = world.getBiomeGenForCoords(randX, randZ);
				if (rainHeight <= y + renderDistance && rainHeight >= y - renderDistance && biome.getFloatTemperature(randX, rainHeight, randZ) < 0.2F)
				{
					float f1 = this.random.nextFloat();
					float f2 = this.random.nextFloat();
					if (block != null)
					{
						if (block.getMaterial() != Material.lava)
						{
							if (this.random.nextInt(++l) == 0)
							{
								d = randX + f1;
								d1 = rainHeight + 0.1F - block.getBlockBoundsMinY();
								d2 = randZ + f2;
							}
						}
					}
				}
			}
			if (l > 0 && this.random.nextInt(3000) < this.snowSoundCounter++)
			{
				this.snowSoundCounter = 0;
				if (d1 <= cam.posY + 1.0D || world.getPrecipitationHeight(MathHelper.floor_double(cam.posX), MathHelper.floor_double(cam.posZ)) <= MathHelper.floor_double(cam.posY))
				{
					world.playSound(d, d1, d2, "christmascraft:snow1", 0.05F, 1.0F, false);
				}
			}
		}
	}
}