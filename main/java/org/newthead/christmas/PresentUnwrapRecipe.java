package org.newthead.christmas;

import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class PresentUnwrapRecipe implements IRecipe
{
	@Override
	public boolean matches(InventoryCrafting var1, World world)
	{
		ItemStack presentFound = null;
		for (int i = 0; i < 9; i++)
		{
			ItemStack slotItem = var1.getStackInSlot(i);
			if (slotItem != null && slotItem.getItem() == Item.getItemFromBlock(ChristmasCraft.blockPresent))
			{
				if (presentFound != null) {
					return false;
				}
				presentFound = slotItem;
			}
		}
		return (presentFound != null) && (presentFound.hasTagCompound()) && (presentFound.stackTagCompound.getTag("giftcontents") != null);
	}

	@Override
	public ItemStack getCraftingResult(InventoryCrafting var1)
	{
		ItemStack foundPresent = null;
		for (int i = 0; i < 9; i++)
		{
			ItemStack slotItem = var1.getStackInSlot(i);
			if (slotItem != null) {
				if (slotItem.getItem() == Item.getItemFromBlock(ChristmasCraft.blockPresent))
				{
					if (foundPresent != null)
					{
						return null;
					}
					foundPresent = slotItem;
				}
				else
				{
					return null;
				}
			}
		}
		if (foundPresent == null)
		{
			return null;
		}
		ItemStack contents = null;
		if (foundPresent.hasTagCompound())
		{
			NBTTagCompound contentTag = (NBTTagCompound)foundPresent.stackTagCompound.getTag("giftcontents");
			contents = ItemStack.loadItemStackFromNBT(contentTag);
		}
		return contents;
	}

	@Override
	public int getRecipeSize()
	{
		return 1;
	}

	@Override
	public ItemStack getRecipeOutput()
	{
		return null;
	}
}
